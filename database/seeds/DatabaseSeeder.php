<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CategoryTableSeeder');
		$this->call('ProductTableSeeder');
		$this->call('VideoTableSeeder');
		$this->call('EmailMessageTableSeeder');
		$this->call('WebsiteContentTableSeeder');

        $this->command->info('All tables are seeded!');
	}

}
