<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->truncate();

        $category = new Category;

        Category::create([
        	'title' => 'Basic Control',
        	'description' => 'Still using your girlfriend’s skin products? NOT the answer to your oily skin. Try our Basic Control line – specifically formulated for men to look clean and polished, minus the shine.',
        	'image' => 'productCategories/basic-care.png',
        	'background_image' => 'img/bg-products-basic-care.jpg',
        	'status' => 0,
        	'is_active' => 1
        ]);
        Category::create([
            'title' => 'Acne Control',
            'description' => 'Face your pimple problems like a grown man should. Our Acne Control line helps treat Face and Body Acne and lightens marks along the way so you can look undoubtedly cool.',
            'image' => 'productCategories/acne-control.png',
            'background_image' => 'img/bg-products-acne-control.jpg',
            'status' => 1,
            'is_active' => 1
        ]);
        Category::create([
        	'title' => 'Whitening',
        	'description' => 'Wanting fairer skin doesn\'t make you any less of a man. Our Whitening line helps remove stubborn dark spots to allow any guy achieve clearer, more confident-looking skin.',
        	'image' => 'productCategories/whitening.png',
        	'background_image' => 'img/bg-products-whitening.jpg',
        	'status' => 0,
        	'is_active' => 1
        ]);
    }

}
