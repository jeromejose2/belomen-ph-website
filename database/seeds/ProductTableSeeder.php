<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        
        DB::table('products')->truncate();

        Product::create([
        	'title' => 'WHITENING BODY BAR',
        	'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
        	'price' => 62.75,
            'content' => 135,
        	'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
        	'image' => 'products/whitening-face-bar.png',
        	'link' => 'http://www.lazada.com.ph/belo-men-whitening-body-bar-135g-185908.html?mp=1',
            'category_id' => 1,
            'order' => 1,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'ACNE CONTROL WHITENING BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 100,
            'content' => 100,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/acne-control-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-acne-control-whitening-bar-100g-348744.html?mp=1',
            'category_id' => 2,
            'order' => 1,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'DEEP CLEANSING BODY BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 60.75,
            'content' => 135,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/deep-cleansing-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-deep-cleansing-body-bar-135g-185902.html?mp=1',
            'category_id' => 3,
            'order' => 1,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'WHITENING BODY BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 62.75,
            'content' => 135,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/whitening-face-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-whitening-body-bar-135g-185908.html?mp=1',
            'category_id' => 1,
            'order' => 2,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'ACNE CONTROL WHITENING BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 100,
            'content' => 100,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/acne-control-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-acne-control-whitening-bar-100g-348744.html?mp=1',
            'category_id' => 2,
            'order' => 2,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'DEEP CLEANSING BODY BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 60.75,
            'content' => 135,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/deep-cleansing-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-deep-cleansing-body-bar-135g-185902.html?mp=1',
            'category_id' => 3,
            'order' => 2,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'WHITENING BODY BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 62.75,
            'content' => 135,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/whitening-face-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-whitening-body-bar-135g-185908.html?mp=1',
            'category_id' => 1,
            'order' => 3,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'ACNE CONTROL WHITENING BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 100,
            'content' => 100,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/acne-control-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-acne-control-whitening-bar-100g-348744.html?mp=1',
            'category_id' => 2,
            'order' => 3,
            'is_active' => 1
        ]);
        Product::create([
            'title' => 'DEEP CLEANSING BODY BAR',
            'description' => '- Whitens in as fast as 7 days with Dermwhite PLUS - a powerful combination of Glutathione, Kojic Acid, and Gigawhite <br /> - Invigorates with skin energizers <br /> - Moisturizes with skin vitamins <br /> - Hypoallergenic and dermatologist-tested',
            'price' => 60.75,
            'content' => 135,
            'analytics' => " ga('send', 'event', 'whitening-body-bar', 'click', 'whitening-body-bar') ",
            'image' => 'products/deep-cleansing-bar.png',
            'link' => 'http://www.lazada.com.ph/belo-men-deep-cleansing-body-bar-135g-185902.html?mp=1',
            'category_id' => 3,
            'order' => 3,
            'is_active' => 1
        ]);
    }

}
