<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\EmailSender;
use App\EmailMessage;
use App\CmsUser;

class EmailMessageTableSeeder extends Seeder {

    public function run()
    {
        DB::table('email_messages')->truncate();

        EmailMessage::create([
            'subject' => 'We got your message!',
            'type' => 'Product Related',
            'content' => '<br> <br> This is to confirm that we have received your message regarding your concern. We are currently looking into it so please expect a reply within (x days).<br> Thank you and we hope for your continued support of BELO MEN products!<br> If you want to reach us elsewhere, you can find us on (SOCIAL MEDIA LINKS ​<br> <br> Thank you,<br> The Belo Men team.'
        ]);

          EmailMessage::create([
            'subject' => 'We got your message!',
            'type' => 'Sales and Distribution',
            'content' => '<br> <br> This is to confirm that we have received your message regarding your concern. We are currently looking into it so please expect a reply within (x days).<br> Thank you and we hope for your continued support of BELO MEN products!<br> If you want to reach us elsewhere, you can find us on (SOCIAL MEDIA LINKS ​<br> <br> Thank you,<br> The Belo Men team.'
        ]);

            EmailMessage::create([
            'subject' => 'We got your message!',
            'type' => 'Belo Medical Group',
            'content' => '<br> <br> This is to confirm that we have received your message regarding your concern. We are currently looking into it so please expect a reply within (x days).<br> Thank you and we hope for your continued support of BELO MEN products!<br> If you want to reach us elsewhere, you can find us on (SOCIAL MEDIA LINKS ​<br> <br> Thank you,<br> The Belo Men team.'
        ]);


         DB::table('cms_users')->truncate();
//49d7c328b368340d6c6e90029931ecdd8a480046 == P@ssw0rd
        CmsUser::create([
            'username' => 'admin',
            'password' => '49d7c328b368340d6c6e90029931ecdd8a480046',
            'is_active' => '1'
        ]);
         CmsUser::create([
            'username' => 'jerome.jose@nuworks.ph',
            'password' => '49d7c328b368340d6c6e90029931ecdd8a480046',
            'is_active' => '1'
        ]);



           DB::table('email_sender')->truncate();
         
        EmailSender::create([
            'email' => 'belomen@iskincareinc.com',
            'name' => 'Lorem Ipsum',
            'type' => 'Product Related',
            'is_active' => '1'
        ]);

   EmailSender::create([
            'email' => 'belomen@iskincareinc.com',
            'name' => 'Lorem Ipsum',
            'type' => 'Sales and Distribution',
            'is_active' => '1'
        ]);
      EmailSender::create([
            'email' => 'belomen@iskincareinc.com',
            'name' => 'Lorem Ipsum',
            'type' => 'Belo Medical Group',
            'is_active' => '1'
        ]);
      
    }

}
