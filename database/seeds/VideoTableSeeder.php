<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Video;

class VideoTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        
        DB::table('videos')->truncate();

        Video::create([
            'title' => 'Uptown Funk',
            'thumbnail' => 'videos/thumb-entry01.png',
            'url' => 'https://www.youtube.com/embed/29QiqIFU_Z4',
            'video_time' => '340',
            'views' => 100,
            'rating' => 3,
            'is_active' => 1
        ]);
        Video::create([
            'title' => 'Sugar',
            'thumbnail' => 'videos/thumb-entry01.png',
            'url' => 'https://www.youtube.com/embed/Yz84MqneBzM',
            'video_time' => '351',
            'views' => 120,
            'rating' => 4,
            'is_active' => 1
        ]);
        Video::create([
            'title' => 'Shake it off',
            'thumbnail' => 'videos/thumb-entry01.png',
            'url' => 'https://www.youtube.com/embed/3tO9klCxWos',
            'video_time' => '357',
            'views' => 1000,
            'rating' => 5,
            'is_active' => 1
        ]);
        Video::create([
            'title' => 'Bang Bang',
            'thumbnail' => 'videos/thumb-entry01.png',
            'url' => 'https://www.youtube.com/embed/7JsIcpaQPps',
            'video_time' => '324',
            'views' => 900,
            'rating' => 2,
            'is_active' => 1
        ]);

    }

}
