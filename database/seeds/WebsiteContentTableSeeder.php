<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\WebsiteContent;

class WebsiteContentTableSeeder extends Seeder {

    public function run()
    {
        Model::unguard();
        
        DB::table('website_contents')->truncate();

        WebsiteContent::create([
        	'key' => 'buy_now_button',
        	'location' => 'header',
            'content' => 'http://www.lazada.com.ph/belo-cosmetics/',
            'description' => 'Buy now button on the header'
        ]);
        WebsiteContent::create([
            'key' => 'terms_and_condition',
            'location' => 'popup',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem</p>',
            'description' => 'terms and condition content for popup'
        ]);
        WebsiteContent::create([
            'key' => 'privacy_policy',
            'location' => 'popup',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem</p>',
            'description' => 'terms and condition content for popup'
        ]);
        WebsiteContent::create([
            'key' => 'office_address',
            'location' => 'header_contact_us',
            'content' => '<span>Unit 605, 6th floor, Ecoplaza Building 2305</span>
                            <span>Don Chino Roces Avenue Extension (Pasong Tamo Extn.)</span>
                            <span>Makati City, Philippines 1231</span>',
            'description' => 'office address for contact us form'
        ]);
        WebsiteContent::create([
            'key' => 'telephone',
            'location' => 'header_contact_us',
            'content' => '<span>+632 659 5534</span>',
            'description' => 'telephone number for contact us form'
        ]);
        WebsiteContent::create([
            'key' => 'concern',
            'location' => 'contact_us_form_select_option',
            'content' => 'Product Related',
            'description' => 'select option'
        ]);
        WebsiteContent::create([
            'key' => 'concern',
            'location' => 'contact_us_form_select_option',
            'content' => 'Sales and Distribution',
            'description' => 'select option'
        ]);
        WebsiteContent::create([
            'key' => 'concern',
            'location' => 'contact_us_form_select_option',
            'content' => 'Belo Medical Group',
            'description' => 'select option'
        ]);

        WebsiteContent::create([
            'key' => 'faq_questions',
            'location' => 'faq_lytebox',
            'content' => 'What is your name?',
            'description' => 'FAQ Question'
        ]);
        WebsiteContent::create([
            'key' => 'faq_answers',
            'location' => 'faq_lytebox',
            'content' => 'Ram Guiao',
            'description' => 'FAQ Answer'
        ]);
        WebsiteContent::create([
            'key' => 'faq_questions',
            'location' => 'faq_lytebox',
            'content' => 'How old are you?',
            'description' => 'FAQ Question'
        ]);
        WebsiteContent::create([
            'key' => 'faq_answers',
            'location' => 'faq_lytebox',
            'content' => '23 y/o',
            'description' => 'FAQ Answer'
        ]);
        WebsiteContent::create([
            'key' => 'faq_questions',
            'location' => 'faq_lytebox',
            'content' => 'Where do you live?',
            'description' => 'FAQ Question'
        ]);
        WebsiteContent::create([
            'key' => 'faq_answers',
            'location' => 'faq_lytebox',
            'content' => 'Mandaluyong City',
            'description' => 'FAQ Answer'
        ]);
    }

}
