window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
          window.webkitRequestAnimationFrame ||
          window.mozRequestAnimationFrame    ||
          function( callback ){
            window.setTimeout(callback, 1000 / 30);
          };
})();

var stopLooping = 0;

var agentVideo = {
	video:"",
	time:"",
	score:0,
	firstRun:true,
	contentShow:"",
	timerID:0,
	overlayCon:"",
	overlay0:"",
	overlay1:"",
	overlay2:"",
	overlay1First:true,
	overlay2First:true,
	overlayEnter:[true,true,true,true],
	overlays:[
		{ x:32 + "px", y:80 + "px", width:350 + "px", height:500 + "px"},
		{ x:700 + "px", y:80 + "px", width:350 + "px", height:500 + "px"},
		{ x:600 + "px", y:230 + "px", width:360 + "px", height:330 + "px"},
		{ x:680 + "px", y:150 + "px", width:360 + "px", height:330 + "px"},
		{ x:180 + "px", y:330 + "px", width:250 + "px", height:200 + "px"},
		{ x:780 + "px", y:330 + "px", width:270 + "px", height:200 + "px"},

	],
	score:0,
	init:function() {
		agentVideo.video = document.getElementById("agentVideo");


		//agentVideo.time = document.getElementById("time");
		//agentVideo.contentShow = document.getElementById("content");

		agentVideo.overlayCon  = document.getElementById("videoOverlay");
		agentVideo.overlay0  = document.getElementById("overlay0");
		agentVideo.overlay1  = document.getElementById("overlay1");
		agentVideo.overlay2  = document.getElementById("overlay2");
		agentVideo.overlay3  = document.getElementById("overlay3");

		agentVideo.updateOverlay("default");
		//agentVideo.updateOverlay(agentVideo.overlay0.style, { x:0, y:0, width:"1000px", height:"1000px"});

		//agentVideo.contentShow.innerHTML = "CLICK to START VIDEO";

		document.getElementById("interactiveCopyStart").innerHTML = "Please wait while the video is loading";
		agentVideo.video.display = "none";
		var is_firefox = /firefox/i.test(navigator.userAgent);
		var is_safari = /safari/i.test(navigator.userAgent);
		var videoType = "mp4";
		agentVideo.video.type="video/mp4";
		if(is_firefox) {
			videoType = "webm";
			//agentVideo.canplay();

		} else if(is_safari) {
			videoType = "ogv";
			//agentVideo.canplay();
		}
		agentVideo.video.style.display = "none";
		agentVideo.video.autoplay = true;
		agentVideo.video.muted = true;

		agentVideo.updateOverlay(agentVideo.overlay0.style, { x:0, y:0, width:"100%", height:"100%"});


		var currentVideo = "";
		agentVideo.video.oncanplaythrough = function() {
			//agentVideo.canplay();
		}
		console.log(document.body.clientWidth);
		if(document.body.clientWidth < 767) {
			console.log("small screen")
			currentVideo = "public/assets/vendors/agent/videos/agent360x202." + videoType;
			console.log(agentVideo.video.src);
		} else if(document.body.clientWidth < 768) {
			console.log("tablet screen")
			currentVideo ="public/assets/vendors/agent/videos/agent720x404." + videoType;
			agentVideo.video.src =

			console.log(agentVideo.video.src);
		} else {
			console.log("desktop screen")
			//agentVideo.video.src = "videos/agent1080x608." + videoType;
			currentVideo = "public/assets/vendors/agent/videos/agent720x404." + videoType;
		}

		if(videoType == "mp4") {
			agentVideo.video.innerHTML = '<source src="'+currentVideo+'"  type="video/mp4; codecs=avc1.42E01E,mp4a.40.2">'
		} else if(videoType == "ogv") {
			agentVideo.video.innerHTML = '<source src="'+currentVideo+'"  type="video/ogg; codecs=theora,vorbis">'
		} else if(videoType == "webm") {
			agentVideo.video.innerHTML = '<source src="'+currentVideo+'" type="video/webm; codecs=vp8,vorbis">'
		}

		/*
		try {
			$("#videoTemp").load(currentVideo, function(responseTxt, statusTxt, xhr){

		        if(statusTxt == "success"){
		           agentVideo.canplay();



		        }
		        if(statusTxt == "error")
		            alert("Error: " + xhr.status + ": " + xhr.statusText);
		    });
		} catch(e) {
			console.log(e);
		}
		*/
		agentVideo.video.load();



		agentVideo.overlay1.onclick = agentVideo.overlay1.ontouchstart = function() {
			if(!agentVideo.overlay1First)
				return;
			agentVideo.overlay1First = false;
			agentVideo.score += 1;

			//document.getElementById("score").innerHTML = "score: " + agentVideo.score;
		}
		agentVideo.overlay2.onclick = agentVideo.overlay2.ontouchstart = function() {
			if(!agentVideo.overlay2First)
				return;
			agentVideo.overlay2First = false;
			agentVideo.score += 1;

			//document.getElementById("score").innerHTML = "score: " + agentVideo.score;
		}

		var checkBuffer = setInterval(function(){

			if(agentVideo.video) {
				try {
					console.log(agentVideo.video.buffered);
					var buffer = agentVideo.video.buffered.end(0);
					console.log(buffer + " " + agentVideo.video.duration);
					agentVideo.video.currentTime = buffer;
					if(buffer >= agentVideo.video.duration) {
						clearInterval(checkBuffer);
						agentVideo.video.autoplay = false;
						agentVideo.video.currentTime = 0;
						agentVideo.video.style.display = "block";
						agentVideo.video.muted = false;
						agentVideo.pause();
						agentVideo.canplay();
					}
				} catch(e) {
					console.log(e);
					agentVideo.video.autoplay = false;
					agentVideo.video.currentTime = 0;
					agentVideo.video.style.display = "block";
					agentVideo.video.muted = false;
					agentVideo.pause();
					agentVideo.canplay();

				}



				/*
	DOMException: Failed to execute 'end' on 'TimeRanges': The index provided (0) is greater than or equal to the maximum bound (0).
	{message: "Failed to execute 'end' on 'TimeRanges': The index…s greater than or equal to the maximum bound (0).", name: "IndexSizeError",
	code: 1, stack: "Error: Failed to execute 'end' on 'TimeRanges': Th…/belo/jay_corporate/html/js/agent-video.js:146:45", INDEX_SIZE_ERR: 1…}code:
	1message: "Failed to execute 'end' on 'TimeRanges': The index provided (0) is greater than or equal to the maximum bound (0)."name: "IndexSizeError"stack:
	"Error: Failed to execute 'end' on 'TimeRanges': The index provided (0) is greater than or equal to the maximum bound (0).↵ at Error (native)↵
	at http://localhost/nwshare/belo/jay_corporate/html/js/agent-video.js:146:45"__proto__: DOMException
				*/


			}
		}, 500);

		setInterval(function(){
			var currentWidth = $(".container").width();

			var interactiveContainer = document.getElementById("interactiveContainer");
			interactiveContainer.style.msTransform = "scale(" + currentWidth/1280 + ")";
			interactiveContainer.style.webkitTransform = "scale(" + currentWidth/1280 + ")";
			interactiveContainer.style.transform = "scale(" + currentWidth/1280 + ")";


			if(currentWidth < 768) {
				interactiveContainer.style.marginTop = -(1280 - currentWidth)/6 + "px";
				interactiveContainer.style.marginLeft = (-(1280 - currentWidth)/2 - 30) + "px";
			} else {
				interactiveContainer.style.marginTop = "0px";
				interactiveContainer.style.marginLeft = -(1280 - currentWidth)/2 + "px";
			}



		}, 1000);



	},
	canplay:function() {
		agentVideo.video.display = "block";

		document.getElementById("interactiveCopyStart").innerHTML = "Click to start the video";

		agentVideo.overlayCon.onclick = function(e) {
			//agentVideo.video.pause();

			e.preventDefault();

			ga('send', 'event', 'play-interactive-video', 'click', 'play-interactive-video') ;
			if(agentVideo.firstRun) {
				agentVideo.score = 0;
				agentVideo.overlay1First = true;
				agentVideo.overlay2First = true;
				agentVideo.overlayEnter = [true,true,true,true];

				agentVideo.startVideo();

				agentVideo.firstRun = false;



				return;
			}


		}
	},
	pause:function() {
		agentVideo.video.pause();
	},
	play:function() {
		agentVideo.video.play();
	},
	startVideo:function() {
		agentVideo.play();
		agentVideo.startCheck();
	},
	startCheck:function() {
		//agentVideo.timerID = setInterval(agentVideo.checkTime, 1000/24);

		requestAnimFrame(agentVideo.checkTime)
	},
	resetOverlay:function(overlay) {
		overlay.left = "0px";
		overlay.top = "0px";
		overlay.width = "0px";
		overlay.height = "0px";
		overlay.display = "none";
	},
	updateOverlay:function(overlay, data) {
		if(overlay == "default") {

			agentVideo.resetOverlay(agentVideo.overlay0.style);
			agentVideo.resetOverlay(agentVideo.overlay1.style);
			agentVideo.resetOverlay(agentVideo.overlay2.style);
			agentVideo.resetOverlay(agentVideo.overlay3.style);

			return;
		}
		overlay.display = "block";
		overlay.left = data.x;
		overlay.top = data.y;
		overlay.width = data.width;
		overlay.height = data.height;
	},
	checkTime:function() {
		var currentTime = agentVideo.video.currentTime;
		var totalTime = agentVideo.video.duration;
		//var contentShow = agentVideo.contentShow;

		/*
			0:01 - 0:08 seconds (copy placeholder)
			0:22 - 0:24 seconds (clickable products)
			0:33 - 0:35 seconds (clickable products)
			0:39.5 - 0:41 seconds (clickable products)
			0:51 - 0:53 seconds (clickable products)
			0:54 - 1:04 seconds (copy + Facebook connect)

		*/


		//agentVideo.time.innerHTML = currentTime + "/" + totalTime;


		if(currentTime >= 3 && currentTime <= 8.8) {
			//contentShow.innerHTML = "copy placeholder"

			agentVideo.updateOverlay(agentVideo.overlay0.style, { x:0, y:0, width:"100%", height:"100%"});

			document.getElementById("interactiveCopyStart").innerHTML = "Click on the Belo Men product for a chance to win a prize!"

		} else if(currentTime >= 22.8 && currentTime <= 25.33) {
			// /else if(currentTime >= 21.8 && currentTime <= 22.4) {
			//contentShow.innerHTML = "clickable products"
			// "54 604" "580 872"

			if(agentVideo.overlayEnter[0]) {
				agentVideo.overlay1First = true;
				agentVideo.overlay2First = true;
				agentVideo.overlayEnter[0] = false;

				agentVideo.updateOverlay(agentVideo.overlay1.style, agentVideo.overlays[0]);
				agentVideo.updateOverlay(agentVideo.overlay2.style, agentVideo.overlays[1]);
			}






		} else if(currentTime >= 33.69 && currentTime <= 35.6) {
			//else if(currentTime >= 33 && currentTime <= 34.5) {
			//contentShow.innerHTML = "clickable products"

			if(agentVideo.overlayEnter[1]) {
				agentVideo.overlay1First = true;
				agentVideo.overlay2First = true;
				agentVideo.overlayEnter[1] = false;

				agentVideo.updateOverlay(agentVideo.overlay1.style, agentVideo.overlays[2]);

			}



			// "1054 568" "1520 800"
		} else if(currentTime >= 40.49 && currentTime <= 42.42) {
			//else if(currentTime >= 39.5 && currentTime <= 41.2) {
			//contentShow.innerHTML = "clickable products"

			if(agentVideo.overlayEnter[2]) {
				agentVideo.overlay1First = true;
				agentVideo.overlay2First = true;
				agentVideo.overlayEnter[2] = false;

				agentVideo.updateOverlay(agentVideo.overlay1.style, agentVideo.overlays[3]);
			}


		} else if(currentTime >= 52.23 && currentTime <= 54.34) {
			//else if(currentTime >= 51.25 && currentTime <= 53.4) {
			//contentShow.innerHTML = "clickable products"

			if(agentVideo.overlayEnter[3]) {
				agentVideo.overlay1First = true;
				agentVideo.overlay2First = true;
				agentVideo.overlayEnter[3] = false;

				agentVideo.updateOverlay(agentVideo.overlay1.style, agentVideo.overlays[4]);
				agentVideo.updateOverlay(agentVideo.overlay2.style, agentVideo.overlays[5]);
			}


		} else if(currentTime >= 55.5 && currentTime <= 64) {
			//contentShow.innerHTML = "copy + Facebook connect"

			agentVideo.updateOverlay(agentVideo.overlay3.style, { x:0, y:0, width:"100%", height:"100%"});


			if(agentVideo.score == 6) {
				if(stopLooping == 0){
					document.getElementById("interactiveCopy").innerHTML = "MISSION ACCOMPLISHED! Give us your details for a chance to win Belo Men samples and exciting prizes! <br><br><img onclick='fb_login()' src='img/fb-connect.png' style='cursor: pointer' />";
					stopLooping = 1;
				}
			} else {
				document.getElementById("interactiveCopy").innerHTML = "SORRY, BRO! You only got "+ agentVideo.score + " out of 6 clicks. Watch the video and try again!"
			}
		} else if(currentTime >= 64.4 && currentTime <= totalTime) {
			//contentShow.innerHTML = "copy + Facebook connect"
			agentVideo.updateOverlay("default");
			agentVideo.updateOverlay(agentVideo.overlay0.style, { x:0, y:0, width:"100%", height:"100%"});
			document.getElementById("interactiveCopyStart").innerHTML = "REPLAY VIDEO";
			stopLooping = 0;
			agentVideo.firstRun = true;
		} else {
			//contentShow.innerHTML = "none";

			agentVideo.updateOverlay("default");
			//document.getElementById("interactiveCopyStart").innerHTML = "";
			//agentVideo.updateOverlay(agentVideo.overlay0.style, { x:0, y:0, width:"100%", height:"100%"});
		}
		if(currentTime != totalTime)
			requestAnimFrame(agentVideo.checkTime);

	}
};
