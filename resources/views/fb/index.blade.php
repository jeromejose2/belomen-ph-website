<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <title>Belomen Tab</title>
       <link rel="stylesheet" href="{{ asset('assets_fb/assets/css/main.min.css') }}">
	 <link rel="stylesheet" type="text/css" href="{{ asset('assets_fb/assets/font/styles.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!--[if lt IE 10]>
      <p class="browsehappy">
        You are using an <strong>outdated</strong> browser. Please
        <a href="http://browsehappy.com/">upgrade your browser</a> to improve
        your experience.
      </p>
    <![endif]-->


<div class="button-loader">
    <span class="f-buynow"></span>
    <span class="f-buynow-h"></span>
    <span class="f-fb"></span>
    <span class="f-fb-h"></span>
    <span class="f-tw"></span>
    <span class="f-tw-h"></span>
</div>

<section id="header-wrap">
    <div class="container">

        <div class="header cf">
            <a href="javascript:;" class="logo"></a>
            
            <div class="social-wrap cf">
                <a href="https://www.facebook.com/belomen" class="btn fb" target="_blank" onclick="ga('send', 'event', 'visit-facebook', 'click', 'visit-facebook');"></a>
                <a href="https://twitter.com/belo_men" class="btn tw" target="_blank" onclick="ga('send', 'event', 'visit-twitter', 'click', 'visit-twitter');"></a>
            </div>
        
        <div class="banner">
            <img src="{{ asset('assets_fb/assets/img/daniel-header.png') }}">
        </div>

        </div>

    </div>

</section>


<?php
$count_1 = 0;
$count_2 = 0;
$count_3 = 0;
?>



<section id="acnecontrol">
	<div class="container cf">
		<div class="title"> <span>ACNE CONTROL</span> </div>
@foreach ($products as $product)


	@if($product->category_id == 2)

<!--list of item-->
						<div class="item cf">
								<div class="imgholder">
									<!-- <img src="{{ asset('assets_fb/assets/img/sampleImg.png') }}"> -->
									<img src="{{ asset('uploads').'/'.$product->image }}">
								</div>
								<div class="product-info">
									<span class="title">{{ $product->title }}</span>
									<span class="measure">{{ $product->content }}</span>
									<br/>
									<ul class="description">
										{!! html_entity_decode($product->description) !!}
										<!-- <li>Acne control and whitening bar in one</li>
										<li> Helps eliminate pimple and body acne while improving skin clarity</li>
										<li>Lightens pimple marks and stubborn dark spots</li>
										<li>Reduces inflammation and melanin formation</li>
										<li>Clinically proven effective and dermatologist-tested</li> -->
									</ul>
									<ul class="price">
										<li class="cf">
											<span class="inner-measure">{{ $product->content }}</span>
											<span class="price">price</span>
											<span class="value">{{ $product->price }}</span>
										</li>
									</ul>
								</div>
								<div class="trigger">
									<a href="{{ $product->link }}" target="_blank" class="btn buy-now" onclick="{{ $product->analytics }}"></a>		
								</div>
						</div>


	@endif





@endforeach
	</div>
</section>	



<section id="basiccare">
	<div class="container cf">
		<div class="title"> <span>BASIC CONTROL</span> </div>
@foreach ($products as $product)


	@if($product->category_id == 1)

<!--list of item-->
						<div class="item cf">
								<div class="imgholder">
									<!-- <img src="{{ asset('assets_fb/assets/img/sampleImg.png') }}"> -->
									<img src="{{ asset('uploads').'/'.$product->image }}">
								</div>
								<div class="product-info">
									<span class="title">{{ $product->title }}</span>
									<span class="measure">{{ $product->content }}</span>
									<br/>
									<ul class="description">
										{!! html_entity_decode($product->description) !!}
										<!-- <li>Acne control and whitening bar in one</li>
										<li> Helps eliminate pimple and body acne while improving skin clarity</li>
										<li>Lightens pimple marks and stubborn dark spots</li>
										<li>Reduces inflammation and melanin formation</li>
										<li>Clinically proven effective and dermatologist-tested</li> -->
									</ul>
									<ul class="price">
										<li class="cf">
											<span class="inner-measure">{{ $product->content }}</span>
											<span class="price">price</span>
											<span class="value">{{ $product->price }}</span>
										</li>
									</ul>
								</div>
								<div class="trigger">
									<a href="{{ $product->link }}" target="_blank" class="btn buy-now" onclick="{{ $product->analytics }}"></a>		
								</div>
						</div>


	@endif





@endforeach
	</div>
</section>	



<section id="whitening">
	<div class="container cf">
		<div class="title"> <span>WHITENING</span> </div>
@foreach ($products as $product)


	@if($product->category_id == 3)

<!--list of item-->
						<div class="item cf">
								<div class="imgholder">
									<!-- <img src="{{ asset('assets_fb/assets/img/sampleImg.png') }}"> -->
									<img src="{{ asset('uploads').'/'.$product->image }}">
								</div>
								<div class="product-info">
									<span class="title">{{ $product->title }}</span>
									<span class="measure">{{ $product->content }}</span>
									<br/>
									<ul class="description">
										{!! html_entity_decode($product->description) !!}
										<!-- <li>Acne control and whitening bar in one</li>
										<li> Helps eliminate pimple and body acne while improving skin clarity</li>
										<li>Lightens pimple marks and stubborn dark spots</li>
										<li>Reduces inflammation and melanin formation</li>
										<li>Clinically proven effective and dermatologist-tested</li> -->
									</ul>
									<ul class="price">
										<li class="cf">
											<span class="inner-measure">{{ $product->content }}</span>
											<span class="price">price</span>
											<span class="value">{{ $product->price }}</span>
										</li>
									</ul>
								</div>
								<div class="trigger">
									<a href="{{ $product->link }}" target="_blank" class="btn buy-now" onclick="{{ $product->analytics }}"></a>		
								</div>
						</div>


	@endif





@endforeach
	</div>
</section>	
	

    <div class="footer">

    </div>
    <script src="{{ asset('assets_fb/assets/js/main.min.js') }}"></script>

    <div class="container">
    	<div class="main-footer"></div>
    </div>
    
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-40273023-50', 'auto');
	ga('send', 'pageview');
</script>


<div id="fb-root"></div>
<script type="text/javascript">// <![CDATA[
        window.fbAsyncInit = function() {
            FB.init({
                appId : '1559745947581091',
                status : true,
                cookie : true,
                xfbml : true
            });
        };
 
        (function() {
            var e = document.createElement('script');
            e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
            e.async = true;
            document.getElementById('fb-root').appendChild(e);
        }());
// ]]></script>

</body>
</html>
