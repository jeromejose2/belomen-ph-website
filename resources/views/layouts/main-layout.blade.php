<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf_token" content="{{ csrf_token() }}">

        <!--og-->
        <meta property="og:title" content="#StopThatPimple" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="http://www.belomen.com/" />
        <meta property="og:image" content="http://belomen.com/assets/img/og.jpg" />
        <meta property="og:description" content="Agent Belo(Daniel Padilla) is on a mission to fight the spread of pimples. Rally with him as he goes to battle against stubborn elements threatening the clarity of men's skin." />


        <title>@yield('title')</title>

        <link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon" />

        <!--link-->

        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/agent/interactive-css.css') }}"> -->
        <!-- <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/fontawesome.css') }}"> -->
        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/style.css') }}"> -->
        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owlcarousel/owl.carousel.css') }}"> -->
        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/lytebox/css/lytebox.css') }}"> -->

        <!-- CSS FOR BACKEND -->
        <!-- <link href="{{ asset('assets/css/backend.css') }}" rel="stylesheet"> -->
        <!-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"> -->

        <!--build:css assets/css/main.min.css -->
        <!-- <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet"> -->
        <link rel="stylesheet" href="{{ asset('assets/css/main.min.css') }}">
        <!-- /build -->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
        .lazyload,
        .lazyloading {
            opacity: 0;
        }
        .lazyloaded {
            opacity: 1;
            transition: opacity 300ms;
        }
        </style>
    </head>
    <body>
        <!--[if lt IE 10]>
          <p class="browsehappy">
            You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
          </p>
        <![endif]-->
        <div id="clickOverlay" class="ovelay"></div>
        <div class="loader-overlay">
            <span class="gif"></span>
        </div>

        <div id="fb-root"></div>

        <header>
            <div class="topbar">
                <div class="container">
                    <a href="{{ url() }}" class="lazyload logo"></a>

                    <div class="quicklinks">
                        <button class="btn" id="toggleContactUs" onClick="ga('send', 'event', 'contact-us-header', 'click', 'contact-us-header');">
                            <i class="fa fa-envelope"></i> <i class="sbar"></i>
                            <span>CONCACT US</span>
                        </button>
                        <a onClick="ga('send', 'event', 'buy-now-header', 'click', 'buy-now-header');" href="<?php echo trim($buyButton) ?>" target="_blank" class="btn buy">
                            <i class="fa fa-shopping-cart"></i> <i class="sbar"></i>
                            <span> BUY NOW</span>
                        </a>
                        <?php //echo html_entity_decode($buyButton) ?>

                        <button class="btn" id="toggleMenu">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="container bottombar">
                <div class="nav-holder">
                    <nav>
                        <!-- no scrolling -->
                        <span><a href="{{ url() }}">HOME</a></span>
                        <span><a href="#products">PRODUCTS</a></span>
                        <span><a href="#video">VIDEOS</a></span>

                        <span class="search">
                            <form method='GET' action='search'>
                                <input type="text" name='query' value='<?= @$_GET['query'] ?>' placeholder="TYPE IN KEYWORDS HERE">
                                <button type='submit'>
                                    <i class="fa fa-search"></i>
                                </button>
                            </form>
                        <div class="clearfix"></div>
                        </span>
                        <span class="label">SEARCH OUR SITE</span>
                    </nav>
                </div>
            </div>

            <div class="clearfix"></div>
        </header>
        <!-- start contact us -->
        <div class="contactus">
            <div class="container">
            <div class="close">&times;</div>

                <div class="form-container">
                    <h2>
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        SEND US A <em>MESSAGE</em>
                    </h2>

                    <p>
                        Men shouldn’t be afraid to ask questions. If
                        you have comments, suggestions and other queries about BELO MEN Products, write
                        to us and we’ll get back to you as soon as we can!
                    </p>

                    <div id="error" class="errorMsg">asdasd</div>

                    {!! Form::open(['url'=>url('contactUs/send'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'return submit_contact()','enctype'=>'multipart/form-data']) !!}
                        <label>YOUR NAME*</label>
                        <input type="text" placeholder="first name" id='name' name='name' required>

                        <label>EMAIL ADDRESS*</label>
                        <input type="email" placeholder="username@domain.com" id='email' name='email' required>

                        <label>CONCERN*</label>
                        <select  id='concern' name='concern' required>
                            <option value='' >Select your concern</option>
                            @foreach($concerns as $concern)
                            <option value='{{ $concern->content }}'>{{ $concern->content }}</option>
                            @endforeach
                        </select>

                        <label>YOUR MESSAGE*</label>
                        <textarea id='message'  name='message' placeholder="Write your personal message here..." required></textarea>


                        <div class="captcha">
                            <div class="g-recaptcha" data-sitekey="6LdmigkTAAAAALOuNCYo0pVzvA_SZrkiMau-2Lwb"></div>
                        </div>

                        <button type="submit" class="btn">
                            <i class="fa fa-send"></i>
                            <span class="sbar"></span>
                            SUBMIT MESSAGE
                        </button>
                   {!! Form::close() !!}
                <div class="clearfix"></div>
                </div>

                <div class="office">
                    <em>Office Address</em>
                    <?php echo html_entity_decode($office_address); ?>

                    <em>Telephone:</em>
                    <?php echo html_entity_decode($telephone); ?>
                <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="clearfix"></div>
        </div>




        @yield('jumbotron')

        @yield('content')

        @yield('videos')
        <footer>

            <div class="container">
                <div class="logo">Belo Men</div>
                <ul class="sitemap">
                    <li><a href="{{ url() }}">HOME</a></li>
                    <li><a href="{{ url('').'#products' }}">PRODUCTS</a></li>
                    <li class="f-skinanalyzer"><a class="skinanalyzerga" onClick="ga('send', 'event', 'analyze-now', 'click', 'analyze-now');" href="#skinanalyzer">SKIN ANALYZER</a></a>
                    <li><a href="{{ url('/videos') }}">VIDEOS</a></li>
                    <li><a onClick="ga('send', 'event', 'contact-us-footer', 'click', 'contact-us-footer');" id="contactbelonow" href="#contactus">CONTACT US</a></li>
        <!-- <li><a onClick="ga('send', 'event', 'about-belo-footer', 'click', 'about-belo-footer');" id="aboutbelomen" href="#">ABOUT BELO</a></li> -->
                    <div class="clearfix"></div>
                </ul>

                <ul class="social">
                    <li>
                        <a onClick="ga('send', 'event', 'share-facebook', 'click', 'share-facebook');" href="https://www.facebook.com/belomen" class="btn fb" target="_blank"> <span class="fa fa-facebook fa-lg"></span> <i class="sbar"></i> LIKE US ON FACEBOOK</a>
                    </li>

                    <li>
                        <a onClick="ga('send', 'event', 'share-twitter', 'click', 'share-twitter');" href="https://twitter.com/belo_men" class="btn tw" target="_blank"> <span class="fa fa-twitter fa-lg"></span> <i class="sbar"></i> FOLLOW US ON TWITTER</a>
                    </li>

                    <li>
                        <a onClick="ga('send', 'event', 'share-instagram', 'click', 'share-twitter');" href="https://instagram.com/belo_men" class="btn instagram" target="_blank"> <span class="fa fa-instagram fa-lg"></span> <i class="sbar"></i> FOLLOW US ON INSTAGRAM</a>
                    </li>

                    <li>
<a onClick="ga('send', 'event', 'buy-now-footer', 'click', 'buy-now-footer');" href="http://www.zalora.com.ph/catalog/?q=belo+men" class="btn lazada" target="_blank"> <span class="fa fa-shopping-cart fa-lg"></span> <i class="sbar"></i> BUY NOW </a>
                    </li>

                    <div class="clearfix"></div>
                </ul>

            <div class="clearfix"></div>
            </div>

            <div class="footnote">
                <div class="container">

                    <div class="copyright">
                        copyright &copy; 2014 - {{ date('Y') }} Intelligent skin care, inc. All rights reserved
                    </div>

                    <div class="terms">
                        <!-- <a href="javascript:void(0);" id="privacyPolicy"></a> -->
                       <a href="javascript:void(0);" id="termsUse" style="display:none;">TERMS OF USE</a>  <a href="javascript:void(0);" id="privacyPolicy" style="display:none;">PRIVACY POLICY</a> <a href="javascript:void(0);" id="faq">FAQ</a>
                    </div>
                </div>

            <div class="clearfix"></div>
            </div>

        </footer>

        <script type="text/javascript">
            var baseUrl = "{{ url() }}";
        </script>

        <script src="{{ asset('assets/vendors/jquery/jquery-1.11.2.min.js') }}"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

        <script src='https://www.google.com/recaptcha/api.js' async></script>

        <!--original js-->
        <script src="{{ asset('assets/vendors/owlcarousel/owl.carousel.min.js') }}"></script>

        <script src="{{ asset('assets/vendors/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <script src="{{ asset('assets/vendors/belo-main.js') }}"></script>

        <script src="{{ asset('assets/vendors/lytebox/js/lytebox.2.3.js') }}"></script>

        <script src="{{ asset('assets/vendors/imakewebthings/waypoints.js') }}"></script>

        <script src="{{ asset('assets/vendors/section.js') }}"></script>

        <script src="{{ asset('assets/js/backend.js') }}"></script>

        <script src="{{ asset('assets/vendors/scrollga/scrollanalytics.js') }}"></script>

        <!--lazyload test-->
        <script src="{{ asset('assets/vendors/lazysizes.min.js') }}" async=""></script>

        <script src="{{ asset('assets/vendors/agent/agent-video.js') }}"></script>
        <script type="text/javascript">
            var baseUrl = "{{ url() }}";
            agentVideo.init();
        </script>


        <script src="{{ asset('assets/vendors/main.js') }}"></script>
        <!--<script src="{{ asset('assets/js/main.min.js') }}"></script>-->

        <script type="text/javascript">
            $(document).ready(function(){
                $(".longTitle").tooltip();

                //lazyload supports background images
                document.addEventListener('lazybeforeunveil', function(e){
                    var bg = e.target.getAttribute('data-bg');
                    if(bg){
                        e.target.style.backgroundImage = 'url(' + bg + ')';
                    }
                });
            });
        </script>

        <script >
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-42684485-30', 'auto');
              ga('send', 'pageview');
        </script>

        <!-- jerome popup -->
        @if(session()->has('error'))
            <script type="text/javascript">
            alert("{{ session('error')['message'] }}");
            </script>
            {{Session::forget('error')}}
        @elseif(session()->has('success'))
            <script type="text/javascript">
            success_contactus();
            </script>
        @endif
        <!-- jerome popup -->
    </body>
</html>



<!--<script src="{{ asset('assets/vendors/loadcss.js') }}"></script>
<script>
    var GGstylesheet = ['assets/css/main.css',];
    var GGcounter = 0;
    function onloadCSS( ss, callback ) {
        console.log("called",GGcounter);
        ss.onload = function() {
            ss.onload = null;
            if( callback ) {
                callback.call( ss );
            }
        };
        if( "isApplicationInstalled" in navigator && "onloadcssdefined" in ss ) {
            ss.onloadcssdefined( callback );
        }
    }
    function asyncCSS(){
        GGcounter++;
        onloadCSS( loadCSS( GGstylesheet[GGcounter] ), asyncCSS );
    }
    //initial
    onloadCSS( loadCSS( GGstylesheet[GGcounter] ), asyncCSS );
</script>-->
