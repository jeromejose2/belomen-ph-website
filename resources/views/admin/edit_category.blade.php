@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>url('admin/category/update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
						@if(isset($categories))
							<input type="hidden" name="id" value="{{ $categories[0]->id }}">
						@endif
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Title</label>
										<div class="col-md-9">
											<input type="text" class="form-control" placeholder="" name="title" value="{{ isset($categories[0]->title) ? $categories[0]->title : '' }}" pattern=".{5,}"  title="5 characters minimum" required>
											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Description</label>
										<div class="col-md-9">
											<input class="form-control" placeholder="" name="description" value="{{ isset($categories[0]->description) ? $categories[0]->description : '' }}"  pattern=".{5,}"  title="5 characters minimum"  maxlength="140" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<!--/row-->
							<span class="help-block" id="upload-text-container" ></span>
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Image</label>
										<div class="col-md-9">
											<!-- <input type="email" class="form-control" placeholder="" name="email" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											<img id="uploadPreview_image" style="width: 100px; height: 100px;" />
											
											@if(isset($categories))
											{!! Form::file('image',['id'=>'image','onchange'=>'check_file_image(this)']); !!}
											<img id="image_load1" src="{{ url('uploads') .'/'.$categories[0]->image}}" width="200px" height="200px"/>
											@else
											{!! Form::file('image',['required','id'=>'image','onchange'=>'check_file_image(this)']); !!}
											@endif
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
								<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Background Image</label>
										<div class="col-md-9">
											<!-- <input type="email" class="form-control" placeholder="" name="email" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											<img id="uploadPreview_background_image" style="width: 100px; height: 100px;" />

											@if(isset($categories))

											{!! Form::file('background_image',['id'=>'background_image','onchange'=>'check_file(this)']); !!}
											<img  id="image_load2" src="{{ url('assets') .'/'.$categories[0]->background_image}}" width="200px" height="200px"/>
											@else

											{!! Form::file('background_image',['required','id'=>'background_image','onchange'=>'check_file(this)']); !!}
											@endif
											<span class="help-block"></span>

										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Tag as New</label>
										<div class="col-md-9">
											<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											
											<?php echo Form::select('status', array(''=>'','1' => 'New', '0' => 'Old'),isset($categories[0]->status) ? $categories[0]->status : '',['required']); ?>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							

							<div class="col-md-3">
									<div class="form-group">
										<label class="control-label col-md-3">Category Order</label>
										<div class="col-md-9">
											<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->

											 <select name ="order" required>
											    <option value=""> --Select-- </option>
											    <?php for ($i=0; $i < $count + 1  ; $i++) { ?>
											    <option value="<?=$i ?>" <?php echo isset($categories[0]->order)&& $categories[0]->order == $i ? 'selected' : ''; ?>><?=$i?></option>
											    <?php } ?>
											</select>

											
											<?php
											//  echo Form::select('order', array(
											// '0' => '0',
											// '1' => '1',
											// '2' => '2',
											// '3' => '3'

											//  ),isset($categories[0]->order) ? $categories[0]->order : ''); 
											 ?>
							<span class="help-block"></span>
										</div>
									</div>
								</div>
	</div>

							
							

						<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Status</label>
										<div class="col-md-9">
											<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											
											<?php echo Form::select('is_active', array('1' => 'Active', '0' => 'Inactive'),isset($categories[0]->is_active) ? $categories[0]->is_active : ''); ?>
<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/home') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();

		function check_file(oInput)
{
	var _validFileExtensions = [".jpg",'.png','.gif','.jpeg'];
var _element = $('#upload-text-container');  
var file_name = '';
   if (oInput.type == "file") {
   	the_file = oInput.files[0];
   	file_name = the_file.name;
       var sFileName = oInput.value;
        if (sFileName.length > 0) {
           var blnValid = false;
           for (var j = 0; j < _validFileExtensions.length; j++) {
               var sCurExtension = _validFileExtensions[j];
               if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                   blnValid = true;
                   $('#image_load2').hide();
                    var oFReader = new FileReader();
				    oFReader.readAsDataURL(document.getElementById("background_image").files[0]);

				    oFReader.onload = function(oFREvent) {
				      document.getElementById("uploadPreview_background_image").src = oFREvent.target.result;
				    };

                   break;
               }
           }
            
           if (!blnValid) {
          
               _element.css('color', 'red').html("Sorry, " + file_name + " is invalid, allowed extensions is: " + _validFileExtensions.join(", "));
               oInput.value = "";
               return false;
           }
       }
   }
}

		function check_file_image(oInput)
{
	var _validFileExtensions = [".jpg",'.png','.gif','.jpeg'];
var _element = $('#upload-text-container');  
var file_name = '';
   if (oInput.type == "file") {
   	the_file = oInput.files[0];
   	file_name = the_file.name;
       var sFileName = oInput.value;
        if (sFileName.length > 0) {
           var blnValid = false;
           for (var j = 0; j < _validFileExtensions.length; j++) {
               var sCurExtension = _validFileExtensions[j];
               if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                   blnValid = true;
                   $('#image_load1').hide();
                    var oFReader = new FileReader();
				    oFReader.readAsDataURL(document.getElementById("image").files[0]);

				    oFReader.onload = function(oFREvent) {
				      document.getElementById("uploadPreview_image").src = oFREvent.target.result;
				    };
    
                   break;
               }
           }
            
           if (!blnValid) {
          
               _element.css('color', 'red').html("Sorry, " + file_name + " is invalid, allowed extensions is: " + _validFileExtensions.join(", "));
               oInput.value = "";
               return false;
           }
       }
   }
}


	</script>

@stop
