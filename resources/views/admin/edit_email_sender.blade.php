@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
<div class="row">
				<div class="col-md-11 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i> Edit Form
							</div>
							<div class="tools">
								<a href="" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="" class="reload">
								</a>
								<a href="" class="remove">
								</a>
							</div>
						</div>

<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>url('admin/emails/sender/update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
						@if(isset($email))
							<input type="hidden" name="id" value="{{ $email[0]->id }}">
						@endif
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">



										<label class="control-label col-md-3">Email Address</label>
										<div class="col-md-9">

											<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-envelope"></i>
											</span>
											<input type="text" class="form-control" placeholder="Email Address" name="email" value="{{ isset($email[0]->email) ? $email[0]->email : '' }}" required>
										</div>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
						
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">

										<label class="control-label col-md-3">Name</label>
										<div class="col-md-9">
											<div class="input-group">
											<span class="input-group-addon">

													<i class="fa fa-user"></i>
													</span>

									<input class="form-control" placeholder="" name="name" id="name" value="{{ isset($email[0]->name) ? $email[0]->name : '' }}" required>
				
								<span class="help-block"></span>
										</div>
										</div>
										<!-- <label class="control-label col-md-3">Video Time </label>
										<div class="col-md-9">
											<input class="form-control" type="number" placeholder="" id="" name="content" value="{{ isset($email[0]->video_time) ? $email[0]->video_time : '' }}" required>
											<span class="help-block"></span>
										</div> -->
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>

								<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Type</label>
										<div class="col-md-9">
											
				<input class="form-control" placeholder="" disabled name="type" id="type" value="{{ isset($email[0]->type) ? $email[0]->type : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>


<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Status</label>
										<div class="col-md-9">
											<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											
					<?php echo Form::select('is_active', array('1' => 'Active', '0' => 'Inactive'),isset($email[0]->is_active) ? $email[0]->is_active : ''); ?>
<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/emails/sender') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>

			</div>
			</div>
			</div>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
	</script>

<script type="text/javascript">
$(document).ready(function(){
  $('.send-btn').click(function(){            
    $.ajax({
      url: 'login',
      type: "post",
      data: {'email':$('input[name=email]').val(), '_token': $('input[name=_token]').val()},
      success: function(data){
        alert(data);
      }
    });      
  }); 
});
function checkurl()
{
	console.log();
	var url_new = $('#url').val();
	 var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	 var pp = /http:\/\/(?:www.)?(vimeo|youtube).com\/(?:watch\?v=)?(.*?)(?:\z|&)/;
  var matches =  (url_new.match(p)) ? RegExp.$1 : false;
  var matches_vimeo =  (url_new.match(pp)) ? RegExp.$1 : false;
if (matches)
{
	document.getElementById("url_error").style.display="none";
   return true;
}
else 
{
   if(parseVimeo(url_new) == null)
   {
	document.getElementById("url_error").style.display="block";
   	$("#url_error span").text("Invalid Url of "+url_new);
  	$('#url').val('');
   	return false;
   }else
   {
   	document.getElementById("url_error").style.display="none";
	return true;
   }
}

}

function parseVimeo(str) 
{
    // embed & link: http://vimeo.com/86164897
    var re = /\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i;
    var matches = re.exec(str);
    return matches && matches[1];
}
</script>

@stop
