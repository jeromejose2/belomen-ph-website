@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>url('admin/website_contents/update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
						@if(isset($website_contents))
							<input type="hidden" name="id" value="{{ $website_contents[0]->id }}">
						@endif
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Key</label>
										<div class="col-md-9">
						@if(isset($website_contents))
							<input type="text"  class="form-control" disabled placeholder="" name="" value="{{ isset($website_contents[0]->key) ? $website_contents[0]->key : '' }}" required>
							<input type="hidden" name="key" value="{{ $website_contents[0]->key }}">
							@else
									<input type="text"  class="form-control" placeholder="" name="key" value="{{ isset($website_contents[0]->key) ? $website_contents[0]->key : '' }}" required>
						@endif

									
											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Location</label>
										<div class="col-md-9">
											@if(isset($website_contents))
											<input class="form-control" disabled placeholder="" name="" value="{{ isset($website_contents[0]->location) ? $website_contents[0]->location : '' }}" pattern=".{5,}"  title="5 characters minimum" >
											<input type="hidden" name="location" value="{{ $website_contents[0]->location }}">
												@else
<input class="form-control" placeholder="" name="location" value="{{ isset($website_contents[0]->location) ? $website_contents[0]->location : '' }}" pattern=".{5,}"  title="5 characters minimum"  required>
												@endif
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										@if(isset($website_contents) && $website_contents[0]->key == "faq_questions" )
										<label class="control-label col-md-3">Question </label>
										@else
										<label class="control-label col-md-3">Content </label>
										@endif
										<div class="col-md-9">		
										<!-- 	<input class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required> -->

											<!-- <textarea class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required>{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</textarea> -->

											<textarea name="summernote" id="summernote_1">
													<?php echo isset($website_contents[0]->content) ? $website_contents[0]->content : '' ?>
												</textarea>
												
											<span class="help-block"></span>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>

							<!--/row-->
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										@if(isset($website_contents) && $website_contents[0]->key == "faq_questions" )
										<label class="control-label col-md-3">Answer</label>
											@else
										<label class="control-label col-md-3">Description</label>
										@endif
										<div class="col-md-9">
										<!-- <input class="form-control" placeholder="" name="video_time" value="{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}" required> -->

										<textarea name="description" id="summernote_2">
													<?php echo isset($website_contents[0]->description) ? $website_contents[0]->description : '' ?>
												</textarea>

										 <!-- <textarea class="form-control" placeholder="" name="description" value="{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}" required>{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}</textarea> -->
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
						
				
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/website_contents') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>

		<script src="{{ asset('assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
		ComponentsEditors.init();
	</script>
<script type="text/javascript">
$(document).ready(function(){
	
	//$('.note-codable').hide();

});
</script>
@stop
