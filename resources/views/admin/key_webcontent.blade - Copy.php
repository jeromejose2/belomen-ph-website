@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- <label><input type="checkbox" id="autoopen">&nbsp;Auto-open next field</label> -->
					<!-- <label><input type="checkbox" id="inline">&nbsp;Inline editing</label> -->
					<button id="enable" class="btn blue">Enable / Disable</button>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table id="user" class="table table-bordered table-striped">
					<tbody>
						
						@if($key == "buy_now_button")
						 <tr>
						<td style="width:15%">
							Location
						</td>
						<td style="width:50%">
							<a href="javascript:;" id="location" data-type="text" data-pk="{{ isset($website_contents[0]->id) ? $website_contents[0]->id : '' }}" data-original-title="Enter buy button link">
							{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</a>
						</td>
						<td style="width:35%">
							<span class="text-muted">
							Simple text field </span>
						</td>
						</tr>

						 <tr>
						<td style="width:15%">
							 Url Link
						</td>
						<td style="width:50%">
							<a href="javascript:;" id="content" data-type="text" data-pk="{{ isset($website_contents[0]->id) ? $website_contents[0]->id : '' }}" data-original-title="Enter buy button link">
							{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</a>
						</td>
						</tr>

						 <tr>
						<td style="width:15%">
							Button Description
						</td>
						<td style="width:50%">
							<a href="javascript:;" id="description" data-type="text" data-pk="{{ isset($website_contents[0]->id) ? $website_contents[0]->id : '' }}" data-original-title="Enter buy button link">
							{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</a>
						</td>
						</tr>
						

						@endif
					 <tr>
						<td style="width:15%">
							 Username
						</td>
						<td style="width:50%">
							<a href="javascript:;" id="username" data-type="text" data-pk="1" data-original-title="Enter username">
							superuser </a>
						</td>
						<td style="width:35%">
							<span class="text-muted">
							Simple text field </span>
						</td>
					</tr>
					<!--<tr>
						<td>
							 First name
						</td>
						<td>
							<a href="javascript:;" id="firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-original-title="Enter your firstname">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Required text field, originally empty </span>
						</td>
					</tr>
					<tr>
						<td>
							 Sex
						</td>
						<td>
							<a href="javascript:;" id="sex" data-type="select" data-pk="1" data-value="" data-original-title="Select sex">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Select, loaded from js array. Custom display </span>
						</td>
					</tr>
					<tr>
						<td>
							 Group
						</td>
						<td>
							<a href="javascript:;" id="group" data-type="select" data-pk="1" data-value="5" data-source="/groups" data-original-title="Select group">
							Admin </a>
						</td>
						<td>
							<span class="text-muted">
							Select, loaded from server. <strong>No buttons</strong> mode </span>
						</td>
					</tr>
					<tr>
						<td>
							 Status
						</td>
						<td>
							<a href="javascript:;" id="status" data-type="select" data-pk="1" data-value="0" data-source="/status" data-original-title="Select status">
							Active </a>
						</td>
						<td>
							<span class="text-muted">
							Error when loading list items </span>
						</td>
					</tr>
					<tr>
						<td>
							 Plan vacation?
						</td>
						<td>
							<a href="javascript:;" id="vacation" data-type="date" data-viewformat="dd.mm.yyyy" data-pk="1" data-placement="right" data-original-title="When you want vacation to start?">
							25.02.2013 </a>
						</td>
						<td>
							<span class="text-muted">
							Datepicker </span>
						</td>
					</tr>
					<tr>
						<td>
							 Date of birth
						</td>
						<td>
							<a href="javascript:;" id="dob" data-type="combodate" data-value="1984-05-15" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1" data-original-title="Select Date of birth">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Date field (combodate) </span>
						</td>
					</tr>
					<tr>
						<td>
							 Setup event
						</td>
						<td>
							<a href="javascript:;" id="event" data-type="combodate" data-template="D MMM YYYY HH:mm" data-format="YYYY-MM-DD HH:mm" data-viewformat="MMM D, YYYY, HH:mm" data-pk="1" data-original-title="Setup event date and time">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Datetime field (combodate) </span>
						</td>
					</tr>
					<tr>
						<td>
							 Meeting start
						</td>
						<td>
							<a href="javascript:;" id="meeting_start" data-type="datetime" data-pk="1" data-url="/post" data-placement="right" title="Set date & time">
							15/03/2013 12:45 </a>
						</td>
						<td>
							<span class="text-muted">
							Bootstrap datetime </span>
						</td>
					</tr>
					<tr>
						<td>
							 Comments
						</td>
						<td>
							<a href="javascript:;" id="comments" data-type="textarea" data-pk="1" data-placeholder="Your comments here..." data-original-title="Enter comments">awesome<br>
							 user!</a>
						</td>
						<td>
							<span class="text-muted">
							Textarea. Buttons below. Submit by <i>ctrl+enter</i>
							</span>
						</td>
					</tr>
					<tr>
						<td>
							 Type State
						</td>
						<td>
							<a href="javascript:;" id="state" data-type="typeahead" data-pk="1" data-placement="right" data-original-title="Start typing State..">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Bootstrap 2.x typeahead </span>
						</td>
					</tr>
					<tr>
						<td>
							 Fresh fruits
						</td>
						<td>
							<a href="javascript:;" id="fruits" data-type="checklist" data-value="2,3" data-original-title="Select fruits">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Checklist </span>
						</td>
					</tr>
					<tr>
						<td>
							 Tags
						</td>
						<td>
							<a href="javascript:;" id="tags" data-type="select2" data-pk="1" data-original-title="Enter tags">
							html, javascript </a>
						</td>
						<td>
							<span class="text-muted">
							Select2 (tags mode) </span>
						</td>
					</tr>
					<tr>
						<td>
							 Country
						</td>
						<td>
							<a href="javascript:;" id="country" data-type="select2" data-pk="1" data-value="BS" data-original-title="Select country">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Select2 (dropdown mode) </span>
						</td>
					</tr>
					<tr>
						<td>
							 Address
						</td>
						<td>
							<a href="javascript:;" id="address" data-type="address" data-pk="1" data-original-title="Please, fill address">
							</a>
						</td>
						<td>
							<span class="text-muted">
							Your custom input, several fields </span>
						</td>
					</tr>
					<tr>
						<td>
							 Notes
						</td>
						<td>
							<div id="note" data-pk="1" data-type="wysihtml5" data-toggle="manual" data-original-title="Enter notes">
								<h3>WYSIWYG</h3>
								 WYSIWYG means <i>What You See Is What You Get</i>.<br>
								 But may also refer to:
								<ul>
									<li>
										 WYSIWYG (album), a 2000 album by Chumbawamba
									</li>
									<li>
										 "Whatcha See is Whatcha Get", a 1971 song by The Dramatics
									</li>
									<li>
										 WYSIWYG Film Festival, an annual Christian film festival
									</li>
								</ul>
								<i>Source:</i>
								<a href="http://en.wikipedia.org/wiki/WYSIWYG_%28disambiguation%29">
								wikipedia.org </a>
							</div>
						</td>
						<td>
							<a href="javascript:;" id="pencil">
							<i class="fa fa-pencil"></i> [edit] </a>
							<br>
							<span class="text-muted">
							Wysihtml5 (bootstrap only).<br>
							 Toggle by another element </span>
						</td>
					</tr> -->
					</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3>Console <small>(all ajax requests here are emulated)</small></h3>
					<div>
						<textarea id="console" rows="8" style="width: 70%" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
<script src="{{asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- BEGIN PLUGINS USED BY X-EDITABLE -->
<script type="text/javascript" src="{{asset('assets/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.zh-CN.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery.mockjax.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-editable/inputs-ext/address/address.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-editable/inputs-ext/wysihtml5/wysihtml5.js')}}"></script>
<!-- END X-EDITABLE PLUGIN -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{asset('assets/global/scripts/metronic.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/layout/scripts/quick-sidebar.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/layout/scripts/demo.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/pages/scripts/form-editable.js')}}"></script>
@stop
@section('defined-scripts')
	<script>
	Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
FormEditable.init();
	</script>
<script type="text/javascript">
$(document).ready(function(){
	
	//$('.note-codable').hide();

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    

});
</script>
@stop
