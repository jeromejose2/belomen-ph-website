<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Belomen Admin | Dashboard</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
{{-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> --}}
<link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="{{ asset('assets/admin/pages/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ asset('assets/global/css/components.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{ asset('assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
<link rel="shortcut icon" href="{{{ asset('favicon.ico') }}}">


<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap-summernote/summernote.css') }}">
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{{ asset('assets/global/css/components.css') }}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{{ asset('assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->


@yield('styles')

<style>
    /*.green-haze {
        border: 1px solid #f7ec15 !important;
        background-color: #f7ec15 !important;
    }
    .green-haze .portlet-title {
        background-color: #f7ec15 !important;
    }
    .green-haze .caption, .green-haze .caption > i {
        color: black !important;
    }
    li.active > a {
        background-color: #f7ec15 !important;
    }
    li.active > a > span, li.active > a > i {
        color: black !important;
    }*/
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
           <!--      <a href="index.html">
                <img  src="{{ asset('assets/admin/layout/img/logo.png') }}" alt="logo" class="logo-default"/>
                </a> -->
            <a href="{{ url('admin/dashboard')}}">
                <img src="{{ asset('assets/img/belomen-admin.png') }}" alt="logo" class="logo-default"/>
                </a> 
                <div class="menu-toggler sidebar-toggler hide">
                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                  
                     <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <li class="dropdown dropdown-quick-sidebar-toggler">
                        <a href="{{url('admin/home/logout')}}" class="dropdown-toggle">
                        <i class="icon-logout"></i>
                        </a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->



        <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
                <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                <li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler">
                    </div>
                    <!-- END SIDEBAR TOGGLER BUTTON -->
                </li>
                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                <li class="sidebar-search-wrapper hidden-xs">
                    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                    <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                    <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                   <!--  <form class="sidebar-search" action="extra_search.html" method="POST">
                        <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                        </a>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
                            </span>
                        </div>
                    </form> -->
                    <!-- END RESPONSIVE QUICK SEARCH FORM -->
                </li>
                <li class="start" style="background-color: #2b3643;">
                  <a disabled style="cursor: default !important;" href="{{ url('/admin/dashboard') }}">
                    <i class=""></i>
                    <span class="title"></span>
                    <span class=""></span>
                    </a>
                </li>
               


                <li class="start <?php echo ($title == 'Dashboard') ? 'active' :''; ?> ">
                    <a href="{{ url('/admin/dashboard') }}">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    </a>
                </li>
                <li class="start <?php echo ($title == 'Categories') ? 'active' :''; ?> ">
                    <a href="{{ url('/admin/home') }}">
                    <i class="icon-home"></i>
                    <span class="title">Category</span>
                    <span class="selected"></span>
                    </a>
                </li>
                <li class="start <?php echo ($title == 'Videos') ? 'active' :''; ?>">
                    <a href="{{ url('/admin/videos') }}">
                    <i class="icon-puzzle"></i>
                    <span class="title">Videos</span>
                    <span class="selected"></span>
                    </a>
                </li>
                <li class="start <?php  echo ($title == 'Products') ? 'active' :''; ?> ">
                    <a href="{{ url('/admin/products') }}">
                    <i class="icon-basket"></i>
                    <span class="title">Products</span>
                    <span class="selected"></span>
                    </a>
                </li>

                <li class="start <?php  echo ($title == 'Website Content') ? 'active' :''; ?> ">
                    <a href="">
                    <i class="icon-folder"></i>
                    <span class="title">Website Content</span>
                    <span class="selected"></span>
                    </a>
                     <ul class="sub-menu">
                        <li>
                            <a href="{{ url('/admin/website_contents/key/buy_now_button') }}">
                            <i class="icon-home"></i>
                            Buy Button</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/website_contents/key/terms_and_condition') }}">
                            <i class="icon-home"></i>
                            Terms and Condition</a>
                        </li>
                     <!--    <li>
                            <a href="{{ url('/admin/website_contents/key/concern') }}">
                            <i class="icon-home"></i>
                            Inquiry Category</a>
                        </li> -->
                        <li>
                            <a href="{{ url('/admin/website_contents/key/privacy_policy') }}">
                            <i class="icon-home"></i>
                            Privacy Policy</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/website_contents/key/faq_questions') }}">
                            <i class="icon-home"></i>
                            FAQ's</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/website_contents/key/office_address_telephone') }}">
                            <i class="icon-home"></i>
                            Contact Us Details</a>
                        </li>
                                                <li>
                            <a href="{{ url('/admin/about_us') }}">
                            <i class="icon-home"></i>
                            About Us </a>
                        </li>
                    </ul>
                </li>


              <li class="<?php  echo ($title == 'Email Content' || $title == 'Email Sender') ? 'active' :''; ?>">
                    <a href="javascript:;">
                    <i class="icon-basket"></i>
                    <span class="title  ">Email Content</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ url('/admin/emails') }}">
                            <i class="icon-home"></i>
                            Email Template</a>
                        </li>
                    <li>
                            <a href="{{ url('/admin/emails/sender') }}">
                            <i class="icon-basket"></i>
                            Email Sender</a>
                        </li>
                          <!--   <li>
                            <a href="ecommerce_orders_view.html">
                            <i class="icon-tag"></i>
                            Order View</a>
                        </li>
                        <li>
                            <a href="ecommerce_products.html">
                            <i class="icon-handbag"></i>
                            Products</a>
                        </li>
                        <li>
                            <a href="ecommerce_products_edit.html">
                            <i class="icon-pencil"></i>
                            Product Edit</a>
                        </li> -->
                    </ul> 
                </li> 


               <li class="start <?php  echo ($title == 'Admin Setting') ? 'active' :''; ?> ">
                    <a href="{{ url('/admin/settings') }}">
                    <i class="icon-user"></i>
                    <span class="title">Admin Setting</span>
                    <span class="selected"></span>
                    </a>
                </li> 
              
                <!-- BEGIN FRONTEND THEME LINKS -->
               
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->


    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            Widget settings form goes here
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Save changes</button>
                            <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN STYLE CUSTOMIZER -->
          
            <!-- BEGIN PAGE HEADER-->
            <h3 class="page-title">
                {{ $title }} 
                <!-- <small>blank page</small> -->
            </h3>
          
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">

                     @if($errors->any())
                        <div class="alert alert-danger" id="alert-error-laravel">
                            <button class="close" data-close="alert"></button>
                            @foreach ($errors->all() as $error)
                            <span>{{ $error }} </span><br>
                            @endforeach
                             
                        </div>
                     @endif
                   @if(session()->has('error'))
            <div class="alert alert-danger" id="alert-error-laravel">
                <button class="close" data-close="alert"></button>
                <span>
                {{ session('error')['message'] }} </span>
                 {{Session::forget('error')}}
            </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success" id="alert-success-laravel">
                <button class="close" data-close="alert"></button>
                <span>
                {{ session('success')['message'] }} </span>
                {{Session::forget('success')}}
            </div>
        @endif
                </div>
            </div>
            @yield('content')
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->


    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner">
             2015 &copy; BeloMen Admin.
        </div>
        <!-- <div class="page-footer-tools">
            <span class="go-top">
            <i class="fa fa-angle-up"></i>
            </span>
        </div> -->
    </div>
    <!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script> 
<![endif]-->
<script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/backend.js') }}"></script>
<script>
    $('#page-loader').modal('show');
    $(document).ready(function() {
        $('#page-loader').modal('hide');
    });
</script>
<!-- END CORE PLUGINS -->
@yield('plugins')
@yield('scripts')
@yield('defined-scripts')
</body>
<!-- END BODY -->
</html>