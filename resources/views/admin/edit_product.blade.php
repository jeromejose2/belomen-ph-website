@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>


	<!-- BEGIN GLOBAL MANDATORY STYLES -->

@endsection
@section('content')
<div class="portlet-body form">
					<!-- BEGIN FORM-->
			
{!! Form::open(['url'=>url('admin/products/update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'return checksummernote()','enctype'=>'multipart/form-data']) !!}
				@if(isset($categories))
					<input type="hidden" name="id" value="{{ $categories[0]->id }}">
				@endif
		<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Title</label>
										<div class="col-md-9">
											<input type="text" class="form-control" placeholder="" name="title" value="{{ isset($categories[0]->title) ? $categories[0]->title : '' }}" pattern=".{5,}"  title="5 characters minimum"  required>
											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Description</label>
										<div class="col-md-9">
											<textarea name="summernote" id="summernote_1" ><?php echo isset($categories[0]->description) ? html_entity_decode($categories[0]->description) : '' ?></textarea>
											<!-- <input class="form-control" placeholder="" name="description" value="{{ isset($categories[0]->description) ? $categories[0]->description : '' }}" required> -->
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Price</label>
										<div class="col-md-9">
											<input class="form-control" placeholder="" type="number"  min="1" step="any"  name="price" value="{{ isset($categories[0]->price) ? $categories[0]->price : '' }}" required>
											
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Product Size</label>
										<div class="col-md-9">
											<input class="form-control" placeholder=""   name="content" value="{{ isset($categories[0]->content) ? $categories[0]->content : '' }}" pattern=".{2,}"  title="2 characters minimum"  required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Analytics</label>
										<div class="col-md-9">
											<input class="form-control" placeholder="" name="analytics" value="{{ isset($categories[0]->analytics) ? $categories[0]->analytics : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Link</label>
										<div class="col-md-9">
											<input class="form-control" placeholder="" name="link" value="{{ isset($categories[0]->link) ? $categories[0]->link : '' }}" required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>

							<!--/row-->
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Image</label>
										<div class="col-md-9" >
											<!-- <input type="email" class="form-control" placeholder="" name="email" value="{{ isset($row) ? $row->from_email : '' }}" required> -->

											<img id="uploadPreview_image" style="width: 100px; height: 100px;" />

											@if(isset($categories))
											{!! Form::file('image',['id'=>'image','onchange'=>'check_file(this)']); !!}
							<img id="image_load1" src="{{ url('uploads') .'/'.$categories[0]->image}}" width="200px" height="200px"/>
							@else
							{!! Form::file('image',['required','id'=>'image','onchange'=>'check_file(this)']); !!}
											@endif
											<span class="help-block" id="upload-text-container"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
						<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Category ID</label>
												<div class="col-md-9">
													<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
													
													 {!! Form::select('category_id', $categories_id, isset($categories[0]->category_id) ? $categories[0]->category_id : '',['required']) !!}

												<span class="help-block"></span>
												</div>
											</div>
										</div>

								<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3">Product Order</label>
												<div class="col-md-9">
													<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
													
													<?php echo Form::select('order', array(
													'0' => '0',
													'1' => '1',
													'2' => '2',
													'3' => '3'

													 ),isset($categories[0]->order) ? $categories[0]->order : ''); ?>
									<span class="help-block"></span>
												</div>
											</div>
										</div>

										<!--/span-->
										<!--/span-->
										<!--/span-->
							</div>

						<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Status</label>
										<div class="col-md-9">
											<!-- <input type="password" class="form-control" placeholder="" name="password" value="{{ isset($row) ? $row->from_email : '' }}" required> -->
											
											<?php echo Form::select('is_active', array('1' => 'Active', '0' => 'Inactive'),isset($categories[0]->is_active) ? $categories[0]->is_active : ''); ?>
									<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/products') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
						</div>
{!! Form::close() !!}
					<!-- END FORM-->
				
			</div>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
	
    ComponentsEditors.init();
	</script>

<script type="text/javascript">
$(document).ready(function(){
});

function check_file(oInput)
{
	var _validFileExtensions = [".jpg",'.png','.gif','.jpeg'];
var _element = $('#upload-text-container');  
var file_name = '';
   if (oInput.type == "file") {
   	the_file = oInput.files[0];
   	file_name = the_file.name;
       var sFileName = oInput.value;
        if (sFileName.length > 0) {
           var blnValid = false;
           for (var j = 0; j < _validFileExtensions.length; j++) {
               var sCurExtension = _validFileExtensions[j];
               if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                   blnValid = true;

                    $('#image_load1').hide();
                    var oFReader = new FileReader();
				    oFReader.readAsDataURL(document.getElementById("image").files[0]);

				    oFReader.onload = function(oFREvent) {
				      document.getElementById("uploadPreview_image").src = oFREvent.target.result;
				    };

                   break;
               }
           }
            
           if (!blnValid) {
          
               _element.css('color', 'red').html("Sorry, " + file_name + " is invalid, allowed extensions is: " + _validFileExtensions.join(", "));
               oInput.value = "";
               return false;
           }
       }
   }
}




// I needed the opposite function today, so adding here too:
function htmlUnescape(value){
    return String(value)
        .replace("<p>", '')
        .replace("</p>", "")
        .replace("&nbsp;", '')
        .replace("<br>", '')
        .replace("</br>", '');
}

function checksummernote()
{

	var summernote = $('#summernote_1');

		setTimeout(function(){
		console.log(summernote.val().length);
		  console.log(summernote.val());
		  console.log('son'+htmlUnescape(summernote.val()));
	
		 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" 
					  	|| htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
					  {

					  	  setTimeout(function()
					  	    {
					  	  	summernote.focus();
					  	  	$('.note-editable').css("border","1px solid red");
					  	  	$('.note-editable').focus();	
					        }, 1);
					  	  return false;

					  }else
					  {
					  	setTimeout(function(){
									  	 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" || htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
												  {
												  	  setTimeout(function(){
												  	  	summernote.focus();
												  	  	$('.note-editable').css("border","1px solid red");
												  	  	$('.note-editable').focus(); }, 1);
												  	  return false;

												  }else
												  {
												  	$('.note-editable').css("border","1px solid black");
												  			return true;
												  }
									  	}, 1000);
					  }
		}, 1000);

		 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" 
					  	|| htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
					  {
					  	  setTimeout(function(){
					  	  	summernote.focus();
					  	  	$('.note-editable').css("border","1px solid red");
					  	  	$('.note-editable').focus(); }, 1);
					  	  return false;

					  }
	
}
</script>

@stop
