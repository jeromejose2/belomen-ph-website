@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{!! Form::open(['url'=>url('admin/faqs/update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
						@if(isset($website_contents))
							<input type="hidden" name="id" value="{{ $website_contents[0]->id }}">
							<input type="hidden" name="key" value="{{ $website_contents[0]->key }}">
							<input type="hidden" name="location" value="{{ $website_contents[0]->location }}">
						@endif
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Question </label>
										
										<div class="col-md-9">		
										<!-- 	<input class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required> -->

											<!-- <textarea class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required>{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</textarea> -->

{{-- <!-- {!! Form::text('summernote',isset($website_contents[0]->content) ? $website_contents[0]->content : '',['id'=>'summernote_1','pattern'=>'.{5,}','title'=>'5 characters minimum','class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!} --> --}}
{!! Form::text('question',isset($website_contents[0]->content) ? $website_contents[0]->content : '' ,['required','class'=>'form-control','pattern'=>'.{5,}','title'=>'5 characters minimum'])!!}
										
											<!-- <textarea name="summernote" id="summernote_1" style="height: 120px; !important">
													<?php echo isset($website_contents[0]->content) ? $website_contents[0]->content : '' ?>
												</textarea> -->
												
											<span class="help-block"></span>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>

							<!--/row-->
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Answer</label>
											
										<div class="col-md-9">

{{-- <!-- {!! Form::text('description',isset($website_contents[0]->description) ? $website_contents[0]->description : '',['id'=>'summernote_2','pattern'=>'.{5,}','title'=>'5 characters minimum','class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!} --> --}}

{!! Form::text('answer',isset($website_contents[0]->description) ? $website_contents[0]->description : '' ,['required', 'rows' => 2, 'cols' => 40,'class'=>'form-control','pattern'=>'.{5,}','title'=>'5 characters minimum'])!!}

											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
						
				
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/website_contents/key/faq_questions') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					{!! Form::close() !!}
					<!-- END FORM-->
				</div>
			</div>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>

		<script src="{{ asset('assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
		ComponentsEditors.init();
	</script>
<script type="text/javascript">
$(document).ready(function(){
	
	//$('.note-codable').hide();

});
</script>
@stop
