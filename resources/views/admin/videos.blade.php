@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')
	<a class="btn btn-circle green-haze dropdown-toggle" href="{{ url('/admin/videos/edit') }}"> Add Video</a>
	<table id="datatable_credit_memos" class="table table-striped table-bordered table-hover dataTable no-footer" aria-describedby="datatable_credit_memos_info" role="grid">
<thead>
<tr class="heading" role="row">
<th class="sorting_desc" width="5%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1"> Video title </th>
<th class="sorting" width="15%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Url </th>
<th class="sorting" width="15%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Thumbnail </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Views </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Rating </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Actions </th>
</tr>
</thead>

<tbody>
	@foreach ($videos as $video)

<tr class="odd" role="row">
	<td class="sorting_1">{{ $video->title }}</td>
	<td>{{ $video->url }}</td>
	<td>	<img id="image_load1" src="{{ url('uploads') .'/'.$video->thumbnail}}" width="200px" height="200px"/></td>
	<td>{{ $video->views }}</td>
	<td>{{ $video->rating }}</td>
	
	
	<td>
	<a class="btn btn-xs default btn-editable" href="{{ url('/admin/videos/edit').'/'.$video->id}}">
	<i class="fa fa-edit"></i>	Edit </a>
	<a class="btn btn-xs default btn-editable" href="{{ url('/admin/videos/delete').'/'.$video->id}}">
	<i class="fa fa-remove"></i>	Delete </a>
	</td>
</tr>
@endforeach


</tbody>
</table>




@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
	</script>

@stop
