@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')

<div class="row">
				<div class="col-md-9 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase">Website Content 
-
{{ ($website_contents[0]->key == 'buy_now_button') ? 'Buy Button Link' : ''  }}
		{{ ($website_contents[0]->key == 'terms_and_condition') ? 'Terms and Condition' : ''  }}
		{{ ($website_contents[0]->key == 'privacy_policy') ? 'Privacy Policy' : ''  }}
		{{ ($website_contents[0]->key == 'office_address') ? 'Office Address' : ''  }}
		{{ ($website_contents[0]->key == 'telephone') ? 'Office Telephone' : ''  }}
		{{ ($website_contents[0]->key == 'concern') ? 'Contact Us' : ''  }}
		{{ ($website_contents[0]->key == 'faq_questions') ? 'FAQ Question' : ''  }}
		{{ ($website_contents[0]->key == 'faq_answers') ? 'FAQ Answer' : ''  }}
								</span>
							</div>
							<!-- <div class="actions">
								<div class="btn-group">
									<a class="btn btn-sm green dropdown-toggle" href="javascript:;" data-toggle="dropdown">
									Actions <i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="javascript:;">
											<i class="fa fa-pencil"></i> Edit </a>
										</li>
										<li>
											<a href="javascript:;">
											<i class="fa fa-trash-o"></i> Delete </a>
										</li>
										<li>
											<a href="javascript:;">
											<i class="fa fa-ban"></i> Ban </a>
										</li>
										<li class="divider">
										</li>
										<li>
											<a href="javascript:;">
											<i class="i"></i> Make admin </a>
										</li>
									</ul>
								</div>
							</div> -->
						</div>
						

<div class="portlet-body form">
					<!-- BEGIN FORM-->
{!! Form::open(['url'=>url('admin/website_contents/key_update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'javascript:return checksummernote()','enctype'=>'multipart/form-data']) !!}


						@if(isset($website_contents))
							<input type="hidden" name="id" value="{{ $website_contents[0]->id }}">
						@endif
						<div class="form-body">
							<div class="alert alert-danger {{ !isset($error_message) ? 'display-hide' : '' }}" id="error">
								<button class="close" data-close="alert"></button>
								<span>
									{{ isset($error_message) ? $error_message : '' }}
								 </span>
							</div>
							@if(isset($success_message))
								<div class="alert alert-success" id="success">
									<button class="close" data-close="alert"></button>
									<span>
										{{ $success_message }}
									 </span>
								</div>
							@endif
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Key</label>
										<div class="col-md-9">
						@if(isset($website_contents))
							<input type="text"  class="form-control" disabled placeholder="" name="" value="{{ isset($website_contents[0]->key) ? $website_contents[0]->key : '' }}" required>
							<input type="hidden" name="key" value="{{ $website_contents[0]->key }}">
							@else
									<input type="text"  class="form-control" placeholder="" name="key" value="{{ isset($website_contents[0]->key) ? $website_contents[0]->key : '' }}" required>
						@endif

									
											<span class="help-block">
											</span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
							</div>
							<!--/row-->
							<input type="hidden" name="location" value="{{ isset($website_contents[0]->location) ? $website_contents[0]->location : ''  }}">
<!-- 
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Location</label>
										<div class="col-md-9">
											<input class="form-control" placeholder="" name="location" value="{{ isset($website_contents[0]->location) ? $website_contents[0]->location : '' }}" pattern=".{5,}"  title="5 characters minimum"  required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div> -->
								@if($key == "buy_now_button")

								<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Button URL</label>
										<div class="col-md-9">
<input type="url" class="form-control" placeholder="" name="summernote" value="{{ isset($website_contents[0]->content) ? trim($website_contents[0]->content) : '' }}" pattern=".{5,}"  title="5 characters minimum"  required>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>

								@else
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Content </label>
										<div class="col-md-9">
										<!-- 	<input class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required> -->

											<!-- <textarea class="form-control" placeholder="" name="content" value="{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}" required>{{ isset($website_contents[0]->content) ? $website_contents[0]->content : '' }}</textarea> -->

												<textarea name="summernote" id="summernote_1">
													<?php echo isset($website_contents[0]->content) ? $website_contents[0]->content : '' ?>
												</textarea>
												
											<span class="help-block"></span>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>


								@endif

							<!--/row-->
							<!--/row-->
							<div class="row">
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-3">Description</label>
										<div class="col-md-9">
										<!-- <input class="form-control" placeholder="" name="video_time" value="{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}" required> -->


										 <textarea class="form-control" placeholder="" name="description" value="{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}" required>{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}</textarea>
											<span class="help-block"></span>
										</div>
									</div>
								</div>
								<!--/span-->
								<!--/span-->
								<!--/span-->
							</div>
						
				
							<!--/row-->
						<div class="form-actions">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn yellow-lemon">Submit</button>
											<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
											<a href="{{ url('admin/website_contents') }}" class="btn default">Cancel</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
								</div>
							</div>
						</div>
					
					<!-- END FORM-->
				</div>
				{!! Form::close() !!}
			</div>
</div>



@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>

		<script src="{{ asset('assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

@stop
@section('defined-scripts')
		<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
	
    ComponentsEditors.init();
	</script>
<script type="text/javascript">
$(document).ready(function(){
	
//	$('.note-codable').hide();

});



function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

// I needed the opposite function today, so adding here too:
function htmlUnescape(value){
    return String(value)
        .replace("<p>", '')
        .replace("</p>", "")
        .replace("&nbsp;", '')
        .replace("<br>", '')
        .replace("</br>", '');
}

function checksummernote()
{

	var summernote = $('#summernote_1');

		setTimeout(function(){
		console.log(summernote.val().length);
		  console.log(summernote.val());
		  console.log('son'+htmlUnescape(summernote.val()));
	
		 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" 
					  	|| htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
					  {

					  	  setTimeout(function()
					  	    {
					  	  	summernote.focus();
					  	  	$('.note-editable').css("border","1px solid red");
					  	  	$('.note-editable').focus();	
					        }, 1);
					  	  return false;

					  }else
					  {
					  	setTimeout(function(){
									  	 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" || htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
												  {
												  	  setTimeout(function(){
												  	  	summernote.focus();
												  	  	$('.note-editable').css("border","1px solid red");
												  	  	$('.note-editable').focus(); }, 1);
												  	  return false;

												  }else
												  {
												  	$('.note-editable').css("border","1px solid black");
												  			return true;
												  }
									  	}, 1000);
					  }
		}, 1000);

		 if(summernote.val() == "<p><br></p>" || summernote.val() == "&nbsp;"|| summernote.val() == "" 
					  	|| htmlUnescape(summernote.val()) == "" || htmlUnescape(summernote.val()) == null )
					  {
					  	  setTimeout(function(){
					  	  	summernote.focus();
					  	  	$('.note-editable').css("border","1px solid red");
					  	  	$('.note-editable').focus(); }, 1);
					  	  return false;

					  }
	
}
</script>
@stop
