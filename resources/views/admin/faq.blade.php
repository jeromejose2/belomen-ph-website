@extends('admin.default')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
	<style>
		a:hover {
			text-decoration: none;
		}
	</style>
@endsection
@section('content')





<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>FaQ/s
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-hover">
									<tr><td>	<a class="btn btn-circle green-haze dropdown-toggle" href="{{ url('/admin/faqs/new') }}"> Add FAQ</a> </td></tr>
								<thead>
								<tr>
<tr class="heading" role="row">
<th class="sorting_desc" width="5%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1"> Key </th>
<th class="sorting" width="15%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Location </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Content </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Description </th>
<th class="sorting" width="10%" tabindex="0" aria-controls="datatable_credit_memos" rowspan="1" colspan="1" > Actions </th>


								</tr>
								</thead>
								<tbody>

	@foreach ($website_contents as $contents)

<tr class="odd" role="row">
	<td class="sorting_1">
		{{ ($contents->key == 'buy_now_button') ? 'Buy Button Link' : ''  }}
		{{ ($contents->key == 'terms_and_condition') ? 'Terms and Condition' : ''  }}
		{{ ($contents->key == 'privacy_policy') ? 'Privacy Policy' : ''  }}
		{{ ($contents->key == 'office_address') ? 'Office Address' : ''  }}
		{{ ($contents->key == 'telephone') ? 'Office Telephone' : ''  }}
		{{ ($contents->key == 'concern') ? 'Contact Us' : ''  }}
		{{ ($contents->key == 'faq_questions') ? 'FAQ Question' : ''  }}
		{{ ($contents->key == 'faq_answers') ? 'FAQ Answer' : ''  }}
	</td>
	<td>{{ $contents->location }}</td>
	<td>	<?php echo html_entity_decode($contents->content) ?></td>
	<td>{{ $contents->description }}</td>
	
	
	<td>
	<a class="btn default btn-xs blue btn-editable" href="{{ url('/admin/website_contents/edit').'/'.$contents->id}}">
	<i class="fa fa-edit"></i>	Edit </a>
	<a class="btn default btn-xs purple" href="{{ url('/admin/website_contents/delete').'/'.$contents->id}}">
	<i class="fa fa-remove"></i>	Delete </a>
	</td> 
</tr>
@endforeach

<tr>
	<!-- <td colspan='5' align="center"><?php echo $website_contents->render(); ?></td> -->
	<td colspan='5' align="center"><?php echo $website_contents->appends([''=>''])->render(); ?></td>

</tr>

										</tbody>
								</table>
							</div>
						</div>
					</div>



@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
@stop
@section('defined-scripts')
	<script>
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		// ChartsAmcharts.init(); // init demo charts

		Index.init();
		// Index.initDashboardDaterange();
		// Index.initJQVMAP();
		// Index.initCalendar();
		// Index.initCharts();
		// Index.initChat();
		Index.initMiniCharts();
		// Tasks.initDashboardWidget();
	</script>

@stop
