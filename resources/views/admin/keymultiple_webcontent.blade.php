@extends('admin.default')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/admin/pages/css/tasks.css') }}">
<style>
a:hover {
	text-decoration: none;
}
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-md-9 ">
		<!-- BEGIN SAMPLE FORM PORTLET-->
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption font-red-sunglo">
					<i class="icon-settings font-red-sunglo"></i>
					<span class="caption-subject bold uppercase">Website Content
						-
						{{ ($website_contents[0]->key == 'buy_now_button') ? 'Buy Button Link' : ''  }}
						{{ ($website_contents[0]->key == 'terms_and_condition') ? 'Terms and Condition' : ''  }}
						{{ ($website_contents[0]->key == 'privacy_policy') ? 'Privacy Policy' : ''  }}
						{{ ($website_contents[0]->key == 'office_address') ? 'Office Address' : ''  }}
						{{ ($website_contents[0]->key == 'telephone') ? 'Office Telephone' : ''  }}
						{{ ($website_contents[0]->key == 'concern') ? 'Contact Us' : ''  }}
						{{ ($website_contents[0]->key == 'faq_questions') ? 'FAQ Question' : ''  }}
						{{ ($website_contents[0]->key == 'faq_answers') ? 'FAQ Answer' : ''  }}
					</span>
				</div>
				<!-- <div class="actions">
					<div class="btn-group">
						<a class="btn btn-sm green dropdown-toggle" href="javascript:;" data-toggle="dropdown">
						Actions <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="javascript:;">
								<i class="fa fa-pencil"></i> Edit </a>
							</li>
							<li>
								<a href="javascript:;">
								<i class="fa fa-trash-o"></i> Delete </a>
							</li>
							<li>
								<a href="javascript:;">
								<i class="fa fa-ban"></i> Ban </a>
							</li>
							<li class="divider">
							</li>
							<li>
								<a href="javascript:;">
								<i class="i"></i> Make admin </a>
							</li>
						</ul>
					</div>
				</div> -->
			</div>


			<div class="portlet-body form">
				<!-- BEGIN FORM-->
				{!! Form::open(['url'=>url('/admin/website_contents/key_update'), 'class'=>'form-horizontal', 'method'=>'POST', 'id'=>'sender-form', 'onsubmit'=>'','enctype'=>'multipart/form-data']) !!}
				<?php $count = 1;?>
				<?php $count_summernote = 1;?>
				<input type="hidden" name="loopform" value="{{ $key }}">
				@foreach($website_contents as $contents)
				<input type="hidden" name="id[]" value="{{ $contents->id }}">
				<h3 class="form-section">
					{{ ($contents->key == 'buy_now_button') ? 'Buy Button Link' : ''  }}
					{{ ($contents->key == 'terms_and_condition') ? 'Terms and Condition' : ''  }}
					{{ ($contents->key == 'privacy_policy') ? 'Privacy Policy' : ''  }}
					{{ ($contents->key == 'office_address') ? 'Office Address' : ''  }}
					{{ ($contents->key == 'telephone') ? 'Office Telephone' : ''  }}
					{{ ($contents->key == 'concern') ? 'Contact Us' : ''  }}
					{{ ($contents->key == 'faq_questions') ? 'FAQ Question' : ''  }}
					{{ ($contents->key == 'faq_answers') ? 'FAQ Answer' : ''  }}
				</h3>


				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">{{ $count }}</label>
							<div class="col-md-9">
								<input type="text"  class="form-control" disabled placeholder="" name="key_daw" value="{{ isset($contents->key) ? $contents->key : '' }}" required>
								<input type="hidden" name="key_loop[]" value="{{ $contents->key }}">




								<span class="help-block">
								</span>
							</div>
						</div>
					</div>
					<!--/span-->
					<!--/span-->
				</div>

				<div class="row">
					<!--/span-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">Location</label>
							<div class="col-md-9">
								<input class="form-control" placeholder="" name="location[]" value="{{ isset($contents->location) ? $contents->location : '' }}" pattern=".{5,}"  title="5 characters minimum"  required>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<!--/span-->
					<!--/span-->
					<!--/span-->
				</div>

				<div class="row">
					<!--/span-->
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-3">{{ ($key == "faq_questions") ? "Question" : "Content" }} </label>
							<div class="col-md-9">
								<!-- <input class="form-control" placeholder="" name="content[]" value="{{ isset($contents->content) ? $contents->content : '' }}" required> -->

						 <!-- <textarea class="form-control" placeholder="" name="content[]" value="{{ isset($contents->content) ? $contents->content : '' }}" required><?php echo isset( $contents->content ) ? trim( $contents->content ) : '' ?>
						</textarea> -->

						<textarea name="content[]" id="summernote_<?php echo  $count_summernote;?>">
							{{ isset($contents->content) ? $contents->content : '' }}
						</textarea>


						<span class="help-block"></span>
						<span class="help-block"></span>
					</div>
				</div>
			</div>
			<!--/span-->
			<!--/span-->
			<!--/span-->
		</div>
		<div class="row">
			<!--/span-->
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-3">{{ ($key == "faq_questions") ? "Answer" : "Description" }}</label>
					<div class="col-md-9">
						<!-- <input class="form-control" placeholder="" name="video_time" value="{{ isset($website_contents[0]->description) ? $website_contents[0]->description : '' }}" required> -->


						<textarea class="form-control" placeholder="" name="description[]" value="{{ isset($contents->description) ? $contents->description : '' }}" required>{{ isset($contents->description) ? $contents->description : '' }}</textarea>




						<span class="help-block"></span>
					</div>
				</div>
			</div>
			<!--/span-->
			<!--/span-->
			<!--/span-->
		</div>
	</div>

	<?php $count++;?>
	<?php $count_summernote++;?>
	@endforeach

	<div class="form-actions">
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn yellow-lemon">Submit</button>
						<!-- <a href="{{ URL::previous() }}" class="btn default">Cancel</a> -->
						<a href="{{ url('admin/website_contents') }}" class="btn default">Cancel</a>
					</div>
				</div>
			</div>
			<div class="col-md-6">
			</div>
		</div>
	</div>

	{!! Form::close() !!}
	<!-- END FORM-->

</div>




@stop

@section('plugins')
<script src="{{ asset('assets/global/plugins/flot/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.pulsate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>


@stop

@section('scripts')
<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>


<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>

@stop
@section('defined-scripts')
<script>
Metronic.init();
Layout.init();
QuickSidebar.init();
Demo.init();
// ChartsAmcharts.init(); // init demo charts

Index.init();
// Index.initDashboardDaterange();
// Index.initJQVMAP();
// Index.initCalendar();
// Index.initCharts();
// Index.initChat();
Index.initMiniCharts();
// Tasks.initDashboardWidget();
ComponentsEditors.init();
</script>
<script type="text/javascript">
$(document).ready(function(){

//	$('.note-codable').hide();

});
</script>
@stop
