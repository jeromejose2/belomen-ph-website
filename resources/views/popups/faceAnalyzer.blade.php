<section class="pskinanalyzer">
<div class="spl-close" onclick="lytebox.close();"></div>

	<div id="flashcontent"></div>
	
</section>

<script src="{{ asset('assets/swf/swfobject.js') }}"></script>

<script type="text/javascript">
	function initFlash() {

	var minimum_flashversion = "10";

    var so = new SWFObject("assets/swf/skin-analyzer.swf?debug=true&forceWebcam=true", "flash", "1000", "570", minimum_flashversion, "#ffffff");
	so.addParam("quality", "high");
	so.addParam("wmode", "transparent");
	so.addParam("salign", "tl");
	so.addParam("scaleMode", "scale");
	so.addParam("allowscriptaccess","always");
	so.useExpressInstall("assets/swf/expressInstall.swf");
    so.write("flashcontent"); 
	
		// check if flash is not embeded
		
	   /* var pat = /expressInstall/;

	    if(!pat.test(jQuery('#flashcontent embed, object').attr("src"))) {
	        
	        if(jQuery("#flashcontent").html() == "") {
	            jQuery('.preloader').hide();
	            jQuery("#flashcontent").html('<div style="padding:250px 0 0; text-align:center;"><strong>Please download the latest flash player</strong><br /><a href="http://www.adobe.com/go/getflashplayer" target="_top"><img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>');
	        }
	    } */

	    var client_flash_version = deconcept.SWFObjectUtil.getPlayerVersion();
		if(client_flash_version.major < minimum_flashversion ){
			jQuery('.preloader').hide();
			jQuery("#flashcontent").html('<div style="padding:250px 0 0; color:#fff; text-align:center;"><strong>Please download the latest flash player</strong><br /><a href="http://www.adobe.com/go/getflashplayer" target="_top"><img src="https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>');
			return false;
		} 
	}

initFlash();

</script>