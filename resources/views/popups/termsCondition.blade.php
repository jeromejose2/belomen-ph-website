<section id="termsCondition">

	<div class="header">Terms &amp; Conditions</div>
	<div class="content">
		<?php echo html_entity_decode($terms); ?>
	</div>

	<a href="javascript:void(0);" class="popup-x" onclick="lytebox.close()"><span class="desx"></span><span>CLOSE</span></a>
</section>

<script type="text/javascript">
$(document).ready( function() {
		var tick = setTimeout(function(){
			clearTimeout(tick);
			lytebox.stabilize();
		}, 100 );
	} );
</script>