@extends('layouts.main-layout')

@section('title', 'Belomen')

@section('jumbotron')
    @parent
    <section id="jumbotron">
        <div class="theater">
            <div class="item learn-the-story active lazyload">
                <div class="container">

                    <div class="banner">
                        <h1>#STOP<em>THAT</em>PIMPLE</h1>

                        <p>Agent Belo (Daniel Padilla) is on a mission to fight the spread of pimples. </p>

                        <p>Rally with him as he goes to battle against stubborn elements threatening the clarity of men's skin.</p>

                        <a class="btn learn" href="http://stopthatpimple.belomen.com/">
                            <i class="fa fa-comments-o fa-lg"></i> <span class="sbar"></span>
                            LEARN THE STORY
                        </a>
                    </div>

                </div>
            </div>
            <div class="item video">
                <div class="container">
                    <div id="interactiveContainer" class="interactive-video-container">
                        <div id="videoOverlay">
                            <div id="overlay0" class="overlay"><span id="interactiveCopyStart"></span></div>
                            <div id="overlay1" class="overlay"></div>
                            <div id="overlay2" class="overlay"></div>
                            <div id="overlay3" class="overlay"><span id="interactiveCopy"></span></div>
                        </div>
                        <!--<div id="score">score: 0</div>-->
                        <!-- <div id="time"></div> -->
                        <div id="videoTemp"></div>
                        <video width="100%" height="100%" id="agentVideo" oncontextmenu="return false">

                          Your browser does not support the video tag.
                        </video>

                        <script type="text/javascript">
                           agentVideo.init()
                        </script>

                        <div id="content"></div>
                    </div>

                </div>
            </div>
        </div>

        <div class="thumbnail-holder">
            <div class="item" data-toggle="video" style="">
                <img class="lazyload" data-src="{{ asset('assets/img/thumbnail-interactive-video.jpg') }}">
                <span>INTERACTIVE VIDEO</span>
            </div>

            <div class="item" data-toggle="learn-the-story">
                <img class="lazyload" data-src="{{ asset('assets/img/thumbnail-story.jpg') }}">
                <span>#STOPTHATPIMPLE</span>
            </div>

            <a href="javascript:void(0);" class="proxy" data-toggle="video" style="display:none !important"></a>
            <a href="javascript:void(0);" class="proxy active" data-toggle="learn-the-story"></a>
        </div>

        <div class="lazyload mask">
            <div class="lazyload left"></div>
            <div class="lazyload right"></div>
        </div>
    </section>
@stop

@section('content')
    <section class="main">
        <!-- start product listing -->
        <div class="row" id="products">
            <div class="product-arrow prev">
                <i class="fa fa-chevron-left"></i>
            </div>
            <div class="product-arrow next">
                <i class="fa fa-chevron-right"></i>
            </div>

            <div class="product-showcase">
                @foreach ($categories as $category)
                    <div class="lazyload item {{ ($category->status==1) ? 'new' : '' }}" style="background-image:url({{ asset('assets/'.$category->background_image) }})">
                        <h1>{{ $category->title }}</h1>
                        <figure>
                            <img class="lazyload" onclick="facebookConnect()" data-src="{{ asset('uploads/'.$category->image) }}">
                            <figcaption>
                                {{ $category->description }}
                            </figcapion>
                        </figure>
                        <button class="btn _getProductInfo" data-info="{{ $category->id }}"> <span class="fa fa-list-alt"></span> <span class="sbar"></span> MORE INFO</button>

                        <div class="clearfix"></div>
                    </div>
                @endforeach
            </div>

        <div class="clearfix"></div>
        </div>
        <!-- end product listing -->

        <!-- start product info -->
        <div class="product-info">
            <div class="close">&times;</div>

            <div class="listing" id="product-sublisting">
                <!-- items are dynamically called. see main.js && products/getproduct.txt -->
            </div>

            <div class="bg-gradient"></div>
        </div>
    <!-- end main -->
    </section>
@stop

@section('videos')
    @parent
    <section id="video">

        <div class="container">

            <!--Video list -->
            <div class="beloTV">

                <div class="title">
                    <span class="iconTV"></span><span>BELO MEN</span><span class="stv"> TV</span>
                </div>
                <div class='videoList'>
                    @foreach($videos as $video)
                    <?php

                    if (strpos($video->url, 'vimeo') > 0)
                            {
                                $vimeourl = substr(parse_url($video->url, PHP_URL_PATH), 1);
                                $urlvimeo = 'http://player.vimeo.com/video/'.$vimeourl.'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff';

                                $video->url = $urlvimeo;

                            }
                    ?>

                        <a href="javascript:void(0);" class="vid-entry" data-videoLink='{{ $video->url }}' data-videoid="{{ $video->id }}">
                            <div class="thumb">
                                <span class="btn-play"></span>
                                <img class="lazyload" data-src="{{ url('uploads') .'/'. $video->thumbnail }}">
                                <div class="clearfix"></div>
                            </div>
                            <div class="description">
                                <span class="vtitle">{{ $video->title }}</span>
                                <i class="fa fa-play"></i><span class="vtime">{{ gmdate("i:s", $video->video_time) }}</span>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </div>

            <!--skin analyzer-->
            <div class="skin-analyzer-cont">
                <div class="title">
                    <span class="SAicon"></span><span>SKIN</span><span class="stv"> ANALYZER</span>
                </div>
                <div class="content">
                    <div class="desc">Time to man up and test your knowledge about your skin. Use our Skin Analyzer and reveal all the answers to your skincare questions now!</div>
                    <div class="illust">
                        <img class="lazyload" data-src="{{ asset('assets/img/analyzer-face.png') }}">
                    </div>
                </div>
                <div class="clearfix"></div>
                <a id="skanlzr" href="#skinanalyzer" onClick="ga('send', 'event', 'analyze-now', 'click', 'analyze-now');" class="btn-analyze"><i class="fa fa-play"></i><span>ANALYZE NOW</span></a>
            </div>

            <div class="clearfix"></div>

            <ul class="sorter-btn-list">
                <li><a class="active filterVideo" href="#" data-filter='recent'>RECENT ADDED</a></li>
                <li><a href="#" class="filterVideo" data-filter='popular'>MOST POPULAR</a></li>
                <li><a href="#" class="filterVideo" data-filter='viewed'>MOST VIEWED</a></li>
                <li><a href="#" class="filterVideo" data-filter='random'>RANDOM</a></li>
                 <li><a href="videos">VIEW ALL</a></li>
            </ul>

        </div>
    </section>

    @if($hastagproduct == 1)
        <script>
        var explode = function(){

        var url = window.location.href + "#products";
        window.location.href = url;
        };
        setTimeout(explode, 2000);

        </script>
    @endif
@stop
