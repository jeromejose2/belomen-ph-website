@extends('layouts.main-layout')

@section('title', 'Belomen Videos')

@section('jumbotron')
    @parent

@stop

@section('content')
<section id="search-content">

	<div class="container">
		<div class="title">
			<i class="icon"></i><span>SEARCH RESULTS</span>
		</div>

		<div class="clearfix"></div>

		<div class="parseholder">
        	<script>
			  (function() {
			    var cx = '000702745293722894071:yscxxl5wcsu';
			    var gcse = document.createElement('script');
			    gcse.type = 'text/javascript';
			    gcse.async = true;
			    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
			        '//cse.google.com/cse.js?cx=' + cx;
			    var s = document.getElementsByTagName('script')[0];
			    s.parentNode.insertBefore(gcse, s);
			  })();
			</script>
			<gcse:searchresults-only></gcse:searchresults-only>
		</div>

	</div>

</section>
@stop

@section('videos')
    @parent

@stop