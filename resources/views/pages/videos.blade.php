@extends('layouts.main-layout')

@section('title', 'Belomen Videos')

@section('jumbotron')
    @parent

@stop

@section('content')
	<section id="video-gallery">	
		<div class="container">
			<div class="title">
				<span class="icon"></span>BELO MEN TV
			</div>

			<ul class="sort-btn">
				<li>
					<a href="#" class="active filterVid" data-filter='recent'>RECENTLY ADDED</a>
				</li>
				
				<li>
					<a href="#" class='filterVid' data-filter='popular'>MOST POPULAR</a>
				</li>
				<li>
					<a href="#" class='filterVid' data-filter='viewed'>MOST VIEWED</a>
				</li>
				<li>
					<a href="#" class='filterVid' data-filter='random'>RANDOM</a>
				</li>
				<div class="clearfix"></div>
			</ul>

			<div class="clearfix"></div>

			<div class="list-wrapper">

				<div id="videoList">
					@foreach($videos as $video)
					<?php 

					if (strpos($video->url, 'vimeo') > 0)
						 	{
						 		$vimeourl = substr(parse_url($video->url, PHP_URL_PATH), 1);
						 		$urlvimeo = 'http://player.vimeo.com/video/'.$vimeourl.'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff';
						 				
						 		$video->url = $urlvimeo;

						    }
					?>
						<a href="javascript:void(0);" class="vitem-entry" data-videolink="{{ $video->url }}" data-videoid="{{ $video->id }}" >
							<div class="imgholder">
								<span class="play-btn"></span>
								<img  src="{{ url('uploads') .'/'.$video->thumbnail}}">
							</div>
								@if(strlen($video->title)>50)
									<span class="innerTitle longTitle" title="{{ $video->title }}">
										{{ substr($video->title, 0, 50) }}...
									</span>
								@else
									<span class="innerTitle" title="{{ $video->title }}">
										{{ $video->title }}
									</span>
								@endif
							</span>
							<span class="time"><i class="fa fa-play"></i>{{ gmdate('i:s', $video->video_time) }}</span>
						</a>
					@endforeach
				</div>
			
				<div class="clearfix"></div>
			</div>

		</div>
	</section>
@stop

@section('videos')
    @parent

@stop