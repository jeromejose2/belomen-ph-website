<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateEmailRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'subject' => 'required|min:3',
            // 'email' => 'required|email|unique:users,email',
            // 'type' => 'required|min:8', date
            'type' => 'required',
            'summernote' => 'required|min:8'
            
		];
	}
	public function messages()
	{
		return [
		 'summernote.required' => 'Body Message must not empty.',

				];
	}

}
