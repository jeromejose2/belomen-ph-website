<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateFaqRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(session('admin')['id'] == 0)
		{
			return false;
		} else {
			return true;
		}
	}
	public function rules()
	{
		return [
           
            'question' => 'required' 
            //,'answer' => 'required'
				];
	}
	public function messages()
	{
		return [
		 
		 'question.required' => 'Question must not empty.'
		 //,'answer.required' => 'Answer must not empty.'
				];
	}

}
