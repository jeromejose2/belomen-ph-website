<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateFaqRequest;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;



use View;

use App\Video;
use Request;
use App\Audit;
use Input;
use Validator;
use Redirect;
use Session;
use App\User;
use App\WebsiteContent;
use File;
use Auth;
class AdminWebContentController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.index'); 
	}

	public function home()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('website_contents')->orderBy('id')->get();
		
		return view('admin.websitecontents')
		->with('title', 'Website Content')
		->with('website_contents', $categories);
	}

	

	public function faq_edit($id = null)
	{
		if($id != 0)
		{
			$categories = DB::table('website_contents')->where('id', $id)->orderBy('id')->get();
			return view('admin.edit_faq')
			->with('title', 'Website Content')
			->with('website_contents', $categories);
		}else
		{
			return view('admin.edit_faq')->with('title', 'Website Content ');
		}

	}

		public function edit($id = null)
	{
		if($id != 0)
		{
		$categories = DB::table('website_contents')->where('id', $id)->orderBy('id')->get();
		return view('admin.edit_webcontent')
		->with('title', 'Website Content')
		->with('website_contents', $categories);
		}else
		{
			return view('admin.edit_webcontent')->with('title', 'Website Content');
		}


	}

	public function faq_update()
	{
		$input = Request::all();

					$Category = New WebsiteContent;
					$Category->key = 'faq_questions';
					$Category->location = 'faq_lytebox';
					$Category->content = Request::input('question');
					$Category->description = Request::input('answer');
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Added Successfully.'
						]);

		$categories = DB::table('website_contents')->where('key', 'faq_questions')->orderBy('id')->get();
		return redirect('admin/website_contents/key/faq_questions');

	}

	public function key_update()
	{
			$input = Request::all();

			if(isset($input['loopform']) && $input['loopform'] != '')
			{

				for ($i=0; $i < count(Request::input('id')) ; $i++) 
				{ 


								$Category = WebsiteContent::find($input['id'][$i]);
								//$Category->key = Request::input('key');
								$Category->location =$input['location'][$i];
								$Category->content = '<span>'.$input['content'][$i].'</span>';
								$Category->description = $input['description'][$i];

								$save = $Category->save();
						

				}


					if($save)
							{

								session()->put('success', [
									'message'=>'Edit Successfully.'
								]);
							return redirect('admin/website_contents/key/'.Request::input('loopform'));	
							}else
							{
								session()->put('error', [
												'message'=>'Error in saving.Please try again.'
											]);
								return redirect('admin/website_contents/key/');	
							}

			}else
			{
					if(isset($input['id']) && $input['id'] != 0)
					{
							

						
							$Category = WebsiteContent::find($input['id']);
							//$Category->key = Request::input('key');
							$Category->location = Request::input('location');
							$Category->content = Request::input('summernote');
							$Category->description = Request::input('description');
							
							$save = $Category->save();
								session()->put('success', [
									'message'=>'Edit Successfully.'
								]);
					}

						if($save)
					{
					return redirect('admin/website_contents/key/'.Request::input('key'));	
					}else
					{
						session()->put('error', [
										'message'=>'Error in saving.Please try again.'
									]);
						return redirect('admin/website_contents/key/');	
					}

			}



	}

		public function key($key = null)
	{

		if($key == 'office_address_telephone')
		{
		$categories = DB::table('website_contents')->where('key', 'office_address')->orWhere('key', 'telephone')->orderBy('id')->get();

		return view('admin.keymultiple_webcontent')
		->with('title', 'Website Content')
		->with('website_contents', $categories)->with('key',$key);

		}

		if($key == 'faq_questions')
		{
		$categories = DB::table('website_contents')->where('key', 'faq_questions')->orderBy('created_at', 'desc')->paginate(10);

		return view('admin.faq')
		->with('title', 'Website Content')
		->with('website_contents', $categories)->with('key',$key);

		}

		if($key != '')
		{
		$categories = DB::table('website_contents')->where('key', $key)->orderBy('id')->get();
		return view('admin.key_webcontent')
		->with('title', 'Website Content')
		->with('website_contents', $categories)->with('key',$key);

		}else
		{
			return view('admin.key_webcontent')->with('title', 'Website Content');
		}


	}

		public function update()
	{
			$input = Request::all();
			if(isset($input['id']) && $input['id'] != 0)
			{
					


					$Category = WebsiteContent::find($input['id']);
					$Category->key = Request::input('key');
					$Category->location = Request::input('location');
					$Category->content = Request::input('summernote');
					$Category->description = Request::input('description');
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);



			}else
			{
					$Category = New WebsiteContent;
					$Category->key = Request::input('key');
					$Category->location = Request::input('location');
					$Category->content = Request::input('summernote');
					$Category->description = Request::input('description');
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);


			}

		if($save)
		{
			if(Request::input('key') =="faq_questions")
			{
					return redirect('admin/website_contents/key/faq_questions');
			}else
			{		return redirect('admin/website_contents');	}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
			return redirect('admin/website_contents');	
		}

	}

		public function delete($id)
	{

		$category = WebsiteContent::find($id);
		if($category)
		{

			// $filename_image = public_path().'/uploads/'.$category->thumbnail;
			// if (File::exists($filename_image))
			// {
			//     File::delete($filename_image);
			// }
			

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'Removed successfully'
						]);
			return redirect()->back();
			//return redirect('admin/website_contents');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect()->back();
		}
		

	}

}
