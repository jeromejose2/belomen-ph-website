<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;
use Input;
use Validator;
use Redirect;
use Request;
use Session;
use App\User;
use File;
use Auth;
use App\Audit;
use App\Category;
use App\WebsiteContent;
class AdminController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public function dashboard()
	{
		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		return view('admin.dashboard')->with('title', 'Dashboard');


	}
	public function index()
	{
		$input = Request::all();

	
		if(!$input)//for login
		{
			return view('admin.index');
		}

		// if(session('admin')['id'] != 0)
		// {
		// 	$this->home();
		// }


		if($input['_token'] && $input['username'] && $input['password'])
		{
		$password = sha1(md5('belomenjerome'.Request::input('password')));
		$user = DB::table('cms_users')->where(['username'=>$input['username'],'password'=>$password,'is_active'=>1])->first();
		
				if($user)
				{
					session()->put('admin', [
							'id'=>$user->id,
							'username'=>$user->username
						]);

					Session::forget('error');
					return redirect('admin/dashboard');
				}else
				{
					session()->put('error', [
							'message'=>'Invalid Username and Password. Please try again.'
						]);

					return redirect('admin/dashboard');
				}
		}else
		{		return view('admin.dashboard'); }
		/*$categories = DB::table('categories')->where('is_active', 1)->orderBy('id')->get();
		$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->limit(3)->get();
		$buyContent = DB::table('website_contents')->where('key', 'buy_now_button')->first();
		$officeContentAddress = DB::table('website_contents')->where('key', 'office_address')->first();
		$telephone = DB::table('website_contents')->where('key', 'telephone')->first();
		$concerns = DB::table('website_contents')->where('key', 'concern')->get();

		return view('admin.index')
				->with('categories', $categories)
				->with('videos', $videos)
				->with('buyButton', $buyContent->content)
				->with('office_address', $officeContentAddress->content)
				->with('telephone', $telephone->content)
				->with('concerns', $concerns);*/
	}
		public function logout()
	{
		Session::flush();
	return	redirect('admin/home');
		//return view('admin.index');
	}

	public function home()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('categories')->orderBy('order')->orderBy('id')->get();
		
		return view('admin.home')
		->with('title', 'Categories')
		->with('categories', $categories);
	}
	public function edit($id = null)
	{
		if($id != 0)
		{
		$categories = DB::table('categories')->where('id', $id)->orderBy('id')->get();
		$count = DB::table('categories')->count();
		return view('admin.edit_category')
		->with('title', 'Categories')
		->with('categories', $categories)
		->with('count', $count);
		}else
		{
			$count = DB::table('categories')->count();
			return view('admin.edit_category')->with('title', 'Categories')->with('count', $count);
		}

	}
	
	public function update()
	{
			$input = Request::all();
			if(isset($input['id']) && $input['id'] != 0)
			{

					$Category = Category::find($input['id']);
					$imageName = '';
					$imageName_bg = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName = $input['id'] . '-pc-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/productCategories/', $imageName);
							 		$Category->image = 'productCategories/'.$imageName;
							}
					}

					if (Request::hasFile('background_image'))
					{
						if (Request::file('background_image')->isValid())
							{
								
							    $imageName_bg = 'bg-'.$input['id'].Input::file('background_image')->getClientOriginalName();
							 	$move1 =   Request::file('background_image')->move(base_path() . '/public/assets/img/', $imageName_bg);
							 	$Category->background_image = 'img/'.$imageName_bg;
							}
					}



					
					$Category->order = Request::input('order');
					$Category->title = Request::input('title');
					$Category->description = Request::input('description');
				
					
					$Category->status = $input['status'];
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);
					
			}else
			{


					$imageName = '';
					$imageName_bg = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName = '-pc-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/productCategories/', $imageName);
							}
					}

					if (Request::hasFile('background_image'))
					{
						if (Request::file('background_image')->isValid())
							{
								
							    $imageName_bg = 'bg-'.Input::file('background_image')->getClientOriginalName();
							 	$move1 =   Request::file('background_image')->move(base_path() . '/public/assets/img/', $imageName_bg);
							}
					}

					$Category = New Category;
					$Category->title = Request::input('title');
					$Category->description = Request::input('description');
					$Category->image = 'productCategories/'.$imageName;
					$Category->order = Request::input('order');
					$Category->background_image = 'img/'.$imageName_bg;
					$Category->status = $input['status'];
					$Category->is_active = $input['is_active'];
					
					session()->put('success', [
							'message'=>'Added Successfully.'
						]);
					$save = $Category->save();
			}
		
		if($save)
		{
		return redirect('admin/home');	
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		}
	}
	public function delete($id)
	{

		$category = Category::find($id);
		if($category)
		{

			$filename_image = public_path().'/uploads/'.$category->image;
			$filename_image_bg = public_path().'/assets/'.$category->background_image;
			if (File::exists($filename_image))
			{
			    File::delete($filename_image);
			}
			if (File::exists($filename_image_bg))
			{
			    File::delete($filename_image_bg);
			}

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'You successfully removed the category.'
						]);
			return redirect('admin/home');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/home');	
		}
		

	}

	public function export()
	{
		$data = (array) DB::table('tbl_registrants')->get();
		$excel_data[] = array(
			"ID",
			"FACEBOOK ID",
			"FIRST NAME",
			"LAST NAME",
			"GENDER",
			"BIRTH DATE",
			"TIMESTAMP"
		);
		if($data) {
			foreach($data as $k => $v) {
				$excel_data[] = array(
					$v->id,
					$v->facebook_id,
					$v->first_name,
					$v->last_name,
					$v->gender,
					$v->birthday,
					$v->timestamp,
				);
			}
		}

		$this->export_data($excel_data, "Belo_Men_".date("Y-m-dH:i:s"));
	}

	function export_data($rows, $filename="File")
	{
		$headers = "";  
		$data = "";  

		if (count($rows) == 0) {
			echo "<p>The table appears to have no data.</p>";
		} else {
			foreach ($rows[0] as $key => $v) {
				$headers .= $key . "\t";
			}

			foreach ($rows as $row) {
				$line = "";
				foreach($row as $value) {                                            
					if ((!isset($value)) OR ($value == "")) {
						$value = "\t";
					} else {
						$value = str_replace('"', '""', $value);
						$value = '"' . $value . '"' . "\t";
					}
					$line .= $value;
				}
				$data .= trim($line)."\n";
			}

			$data = str_replace("\r","",$data);

			header("Content-type: application/x-msdownload");
			header("Content-Disposition: attachment; filename=$filename.xls");
			echo "$data";
		}
	}

	function aboutus()
	{
		$categories = DB::table('website_contents')->where('key', 'about_us')->orderBy('id','desc')->get();
		$count = DB::table('website_contents')->where('key', 'about_us')->count();
		if(!$categories)
		{
			$categories = ['just to fill up the array if theres no about us'=>''];
		}
			return view('admin.about_us')->with('title', 'About Us')->withProducts($categories);
		
	}

		function aboutus_update()
	{
		$input = Request::all();
		$categories = DB::table('website_contents')->where('key', 'about_us')->orderBy('id','desc')->get();



	

		if(!$categories)
		{
				
					$Category = New WebsiteContent;
					$Category->key = 'about_us';
					$Category->location = 'about us page';
					$Category->description = 'about us page';
					$Category->content = Request::input('summernote');
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);
						return Redirect::back();

		}else
		{

							$Category = WebsiteContent::find($categories[0]->id);
							$Category->content = Request::input('summernote');
							
							$save = $Category->save();
								session()->put('success', [
									'message'=>'Edit Successfully.'
								]);

								return Redirect::back();
		}

		
	}




}
