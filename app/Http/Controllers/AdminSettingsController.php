<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;

use App\User;
use Auth;
use Request;
use App\Audit;
use App\CmsUser;

class AdminSettingsController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
			if(session('admin')['id'] == 0)
			{
				return view('admin.index'); 
			}
		
		//$categories = DB::table('cms_users')->where('is_active', 1)->orderBy('id')->get();
		$categories = CmsUser::all();

		return view('admin.settings')
		->with('title', 'Admin Setting')
		->with('admin_accounts', $categories);


	}

		public function edit($id = null)
	{

		if($id != 0)
		{
		$categories = DB::table('cms_users')->where('id', $id)->orderBy('id')->get();

		return view('admin.edit_admin')
		->with('title', 'Admin Setting')
		->with('admin', $categories);
		}else
		{
			return view('admin.edit_admin')->with('title', 'Products');
		}

	}


		public function update()
	{
			$input = Request::all();
			//49d7c328b368340d6c6e90029931ecdd8a480046 == P@ssw0rd

			if(isset($input['id']) && $input['id'] != 0)
			{
				
					$Category = CmsUser::find($input['id']);

					$Category->username = Request::input('username');

					if(Request::input('password') != 'typeurpass')//default
					{
						$Category->password = sha1(md5('belomenjerome'.Request::input('password')));
					}
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);



			}else
			{
						$Category = New CmsUser;

					$Category->username = Request::input('username');

					if(Request::input('password') != 'typeurpass')//default
					{
						$Category->password = sha1(md5('belomenjerome'.Request::input('password')));
					}
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Add Successfully.'
						]);


			}

		if($save)
		{
		return redirect('admin/settings');	
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		}

	}


	public function delete($id)
	{

		$category = CmsUser::find($id);
		if($category)
		{

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'You successfully removed the admin.'
						]);
			return redirect('admin/settings');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/settings');	
		}
		

	}


	

}
