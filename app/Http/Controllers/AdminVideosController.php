<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;

use App\Video;
use Request;
use App\Audit;
use Input;
use Validator;
use Redirect;
use Session;
use App\User;
use File;
use Auth;
class AdminVideosController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.index'); 
	}

	public function home()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('videos')->orderBy('id','desc')->get();
		
		return view('admin.videos')
		->with('title', 'Videos')
		->with('videos', $categories);
	}

		public function edit($id = null)
	{
		if($id != 0)
		{
		$categories = DB::table('videos')->where('id', $id)->orderBy('id')->get();
		return view('admin.edit_video')
		->with('title', 'Videos')
		->with('videos', $categories);
		}else
		{
			return view('admin.edit_video')->with('title', 'Videos');
		}

	}

	function youtube_id_from_url($url) {
    $pattern = 
        '%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
        ;
    $result = preg_match($pattern, $url, $matches);
    if (false !== $result) {
        return $matches[1];
    }
    return false;
}

function youtube_image($id) {
    $resolution = array (
        '0',
        'sddefault',
        'mqdefault',
        'hqdefault',
        'default'
    );

    for ($x = 0; $x < sizeof($resolution); $x++) {
        $url = 'http://img.youtube.com/vi/' . $id . '/' . $resolution[$x] . '.jpg';
        if (get_headers($url)[0] == 'HTTP/1.0 200 OK') {
            break;
        }
    }
    return $url;
}



		public function update()
	{
			$input = Request::all();

			if(isset($input['id']) && $input['id'] != 0)
			{
					$Category = Video::find($input['id']);

					$imageName = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName = $input['id'] . '-pc-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/videos/', $imageName);
							 		$Category->thumbnail = 'videos/'.$imageName;
							}
					}else
					{
						 $imageName = $input['id'] . '-pc.jpg';

						if (strpos(Request::input('url'), 'youtube') > 0)
						 	{
						 					
						      				//get thumbnail in youtube
											$ch = curl_init($this->youtube_image($this->youtube_id_from_url(Request::input('url'))));
											$fp = fopen(base_path() . '/public/uploads/videos/'.$imageName, 'wb');
											curl_setopt($ch, CURLOPT_FILE, $fp);
											curl_setopt($ch, CURLOPT_HEADER, 0);
											curl_exec($ch);
											curl_close($ch);
											fclose($fp);
											$Category->thumbnail = 'videos/'.$imageName;
												

						    }
						     if (strpos(Request::input('url'), 'vimeo') > 0)
						 	{

						 		$vimeourl = substr(parse_url(Request::input('url'), PHP_URL_PATH), 1);
						 		 $data = file_get_contents("http://vimeo.com/api/v2/video/".$vimeourl.".json");
    							$data = json_decode($data);

    										$ch = curl_init($data[0]->user_portrait_huge);
											$fp = fopen(base_path() . '/public/uploads/videos/'.$imageName, 'wb');
											curl_setopt($ch, CURLOPT_FILE, $fp);
											curl_setopt($ch, CURLOPT_HEADER, 0);
											curl_exec($ch);
											curl_close($ch);
											fclose($fp);
										$Category->thumbnail = 'videos/'.$imageName;


						 	}
					}
					


					if (Request::hasFile('video'))
					{
						if (Request::file('video')->isValid())
							{
								$name_extension = explode('.',$imageName);
								$video_extension = explode('/',Input::file('video')->getMimeType());
								$video_name = $name_extension[0].'.'.$video_extension[1];
								
							 	$move =   Request::file('video')->move(base_path() . '/public/uploads/videos/', $video_name);
							}
					}



				
					$Category->title = Request::input('title');
					
					/* if (strpos(Request::input('url'), 'vimeo') > 0)
						 	{
						 		$vimeourl = substr(parse_url(Request::input('url'), PHP_URL_PATH), 1);
						 		$urlvimeo = 'http://player.vimeo.com/video/'.$vimeourl.'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff';
						 				
						 		$Category->url =$urlvimeo;

						    }else
						    {
						    		$Category->url = Request::input('url');
						    }*/

						    $Category->url = Request::input('url');
					$min=100;
					$max=500;
					//$Category->video_time = rand($min,$max);//Request::input('video_time');
					//$Category->views = rand($min,$max);//Request::input('video_time');
				
					$Category->rating = $input['rating'];
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);



			}else
			{
				$Category = New Video;
				$imageName = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName =uniqid().'vid-pc-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/videos/', $imageName);
							 		$Category->thumbnail = 'videos/'.$imageName;
							}
					}else
					{
						 $imageName = uniqid().'vid-pc.jpg';
						if (strpos(Request::input('url'), 'youtube') > 0)
						 	{
						 					
						      				//get thumbnail in youtube
											$ch = curl_init($this->youtube_image($this->youtube_id_from_url(Request::input('url'))));
											$fp = fopen(base_path() . '/public/uploads/videos/'.$imageName, 'wb');
											curl_setopt($ch, CURLOPT_FILE, $fp);
											curl_setopt($ch, CURLOPT_HEADER, 0);
											curl_exec($ch);
											curl_close($ch);
											fclose($fp);
												$Category->thumbnail = 'videos/'.$imageName;

						    }

						      if (strpos(Request::input('url'), 'vimeo') > 0)
						 	{

						 		$vimeourl = substr(parse_url(Request::input('url'), PHP_URL_PATH), 1);
						 		 $data = file_get_contents("http://vimeo.com/api/v2/video/".$vimeourl.".json");
    							$data = json_decode($data);

    										$ch = curl_init($data[0]->user_portrait_huge);
											$fp = fopen(base_path() . '/public/uploads/videos/'.$imageName, 'wb');
											curl_setopt($ch, CURLOPT_FILE, $fp);
											curl_setopt($ch, CURLOPT_HEADER, 0);
											curl_exec($ch);
											curl_close($ch);
											fclose($fp);
										$Category->thumbnail = 'videos/'.$imageName;


						 	}

						   

					}
					


					if (Request::hasFile('video'))
					{
						if (Request::file('video')->isValid())
							{
								$name_extension = explode('.',$imageName);
								$video_extension = explode('/',Input::file('video')->getMimeType());
								$video_name = $name_extension[0].'.'.$video_extension[1];
								
							 	$move =   Request::file('video')->move(base_path() . '/public/uploads/videos/', $video_name);
							}
					}



				
					$Category->title = Request::input('title');
					$Category->url = Request::input('url');
					$min=100;
					$max=500;
					$Category->video_time = rand($min,$max);//Request::input('video_time');
					$Category->views = rand($min,$max);//Request::input('video_time');
					$Category->rating = $input['rating'];
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);


			}

		if($save)
		{
		return redirect('admin/videos');	
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		}

	}

		public function delete($id)
	{

		$category = Video::find($id);
		if($category)
		{

			$filename_image = public_path().'/uploads/'.$category->thumbnail;
			if (File::exists($filename_image))
			{
			    File::delete($filename_image);
			}
			

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'You successfully removed the video.'
						]);
			return redirect('admin/videos');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/videos');	
		}
		

	}

}
