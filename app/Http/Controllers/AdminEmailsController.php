<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEmailRequest;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


use View;

use App\User;
use Auth;
use Request;
use App\Audit;

use Input;
use Validator;
use Redirect;
use Session;
use App\Category;
use App\EmailMessage;
use App\EmailSender;
use App\WebsiteContent;
use File;

class AdminEmailsController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	

	public function index()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('email_messages')->orderBy('id')->get();
		
		return view('admin.email')
		->with('title', 'Email Content')
		->with('email_messages', $categories);
	}

		public function edit($id = null)
	{

		if($id != 0)
		{
		$categories = DB::table('email_messages')->where('id', $id)->orderBy('id')->get();

		return view('admin.edit_email')
		->with('title', 'Email Content')
		->with('email', $categories);
		}else
		{
			$inquiry_type = DB::table('website_contents')->where('key', 'concern')->orderBy('id')->get();
			return view('admin.edit_email')->with('title', 'Email Content')->with('inquiry_type',json_encode($inquiry_type));
		}

	}


		public function update(CreateEmailRequest $request)
	{


			$input = $request->all();
			$save = false;
			if(isset($input['id']) && $input['id'] != 0)
			{

	
					$Category = EmailMessage::find($input['id']);
					$Category->subject = Request::input('subject');
					$Category->type = Request::input('type');
					$Category->content = trim(Request::input('summernote'));
								
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);
					
			}else
			{
				
				//save inquiry type
				//$WebsiteContent = WebsiteContent::where('LOWER(content)', '=', Request::input('type'))->get();
				//$WebsiteContent = WebsiteContent::select(LOWER(content))->where('content', '=', "'Sales and Distribution'")->get();
				$sql = "select * from website_contents where LOWER(content) = ?";
				$inq_type = DB::select($sql,[strtolower(Request::input('type'))]);
				if(!$inq_type)
				{
					$save = DB::transaction(function()
					{
						$values = array('key' => 'concern',
							             'location' => 'contact_us_form_select_option',
							             'description'=>'select option',
							             'created_at'=>date('Y-m-d H:i:s'),
							             'content'=>ucwords(Request::input('type'))
							             );
						DB::table('website_contents')->insert($values);

						$values1 = array('email' => 'belomen@iskincareinc.com',
							             'name' => 'Admin',
						                 'type'=>ucwords(Request::input('type')),
							             'is_active'=>1,
							             'created_at'=>date('Y-m-d H:i:s')
							             );
						DB::table('email_sender')->insert($values1);



							$Category = New EmailMessage;
							$Category->subject = Request::input('subject');
							$Category->type = ucwords(Request::input('type'));
							$Category->content = trim(Request::input('summernote'));
										
							
							$save = $Category->save();

								

								if($save)
								{
									return true;

								}else
								{
									return false;

								}
							

					});



				}else
				{
					$save = false;
				}

				session()->put('success', [
									'message'=>'Added Successfully.'
								]);


			}
		
				if($save)
				{
							
							return redirect('admin/emails');	
				}else
				{
					session()->put('error', [
									'message'=>'Error in saving.Please try again.'
								]);
					return redirect('admin/emails');	
				}
	}

		public function delete($id)
	{

		$category = EmailMessage::find($id);
		$emailsender = DB::table('email_sender')->where('type',$category->type);

		if($emailsender)
		{
			$emailsender = DB::table('email_sender')->where('type',$category->type)->delete();
		}
		
		if($category)
		{

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'Removed successfully.'
						]);
			return redirect('admin/emails');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/emails');	
		}
		

	}

	public function sender()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('email_sender')->orderBy('id')->get();
		
		return view('admin.sender')
		->with('title', 'Email Sender')
		->with('email_messages', $categories);
	}

		public function sender_edit($id = null)
	{

		if($id != 0)
		{
		$categories = DB::table('email_sender')->where('id', $id)->orderBy('id')->get();

		return view('admin.edit_email_sender')
		->with('title', 'Email Sender')
		->with('email', $categories);
		}else
		{
			return view('admin.edit_email_sender')->with('title', 'Email Content');
		}

	}


		public function sender_update()
	{
			$input = Request::all();
			if(isset($input['id']) && $input['id'] != 0)
			{

	
					$Category = EmailSender::find($input['id']);
					$Category->email = Request::input('email');
					$Category->type = Request::input('type');
					$Category->name = Request::input('name');
					$Category->is_active = Request::input('is_active');
								
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);
					
			}else
			{
					$Category = New EmailSender;
					$Category->email = Request::input('email');
					$Category->type = Request::input('type');
					$Category->name = Request::input('name');
					$Category->is_active = Request::input('is_active');
								
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);

			}
		
		if($save)
		{
		return redirect('admin/emails/sender');	
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		}
	}

		public function sender_delete($id)
	{

		$category = EmailSender::find($id);
		
		if($category)
		{

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'You successfully removed the email sender.'
						]);
			return redirect('admin/emails/sender');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/emails/sender');	
		}
		

	}

}
