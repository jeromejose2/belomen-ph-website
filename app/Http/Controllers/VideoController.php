<?php namespace App\Http\Controllers;
use DB;
use Input;
use Request;

class VideoController extends Controller 
{

	public function index()
	{
		$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->get();
		$websiteContents = DB::table('website_contents')->where('key', 'buy_now_button')->first();
		$buyContent = DB::table('website_contents')->where('key', 'buy_now_button')->first();
		$officeContentAddress = DB::table('website_contents')->where('key', 'office_address')->first();
		$telephone = DB::table('website_contents')->where('key', 'telephone')->first();
		$concerns = DB::table('website_contents')->where('key', 'concern')->get();

		return view('pages.videos')
				->with('videos', $videos)
				->with('buyButton', $buyContent->content)
				->with('office_address', $officeContentAddress->content)
				->with('telephone', $telephone->content)
				->with('concerns', $concerns);
	}

	public function filter()
	{
		$filter = Input::get('filter');
		$type = Input::get('type');

		if($type==1){
			if ($filter == 'recent'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->limit(3)->get();
			} elseif ($filter == 'popular'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('rating', 'DESC')->limit(3)->get();
			} elseif ($filter == 'viewed'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('views', 'DESC')->limit(3)->get();
			} elseif ($filter == 'random'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy(DB::raw('RAND()'))->limit(3)->get();
			}
		}else{
			if ($filter == 'recent'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->get();
			} elseif ($filter == 'popular'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('rating', 'DESC')->get();
			} elseif ($filter == 'viewed'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy('views', 'DESC')->get();
			} elseif ($filter == 'random'){
				$videos = DB::table('videos')->where('is_active', 1)->orderBy(DB::raw('RAND()'))->get();
			}
		}

		echo json_encode($videos);
	}

	/** jerome 07-14-2015 update views field when watch*/
	public function update_views()
	{
		$input = Request::all();
		//$videos = DB::table('videos')->where('id', $input['video_id'])->get();
		if($input['video_id'] != 0)
		{
				$increme = DB::table('videos')->where('id', $input['video_id'])->increment('views', 1);
						
						if($increme)
						{
							echo true;
						}else
						{
							echo false;
						}
		}else
		{
				echo false;
		}
		
	}

}