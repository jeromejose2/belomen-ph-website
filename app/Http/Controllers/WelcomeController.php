<?php namespace App\Http\Controllers;
use DB;

class WelcomeController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = DB::table('categories')->where('is_active', 1)->orderBy('order')->get();
		$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->limit(3)->get();
		$buyContent = DB::table('website_contents')->where('key', 'buy_now_button')->first();
		$officeContentAddress = DB::table('website_contents')->where('key', 'office_address')->first();
		$telephone = DB::table('website_contents')->where('key', 'telephone')->first();
		$concerns = DB::table('website_contents')->where('key', 'concern')->get();

		return view('pages.index')
				->with('categories', $categories)
				->with('videos', $videos)
				->with('buyButton', $buyContent->content)
				->with('office_address', $officeContentAddress->content)
				->with('telephone', $telephone->content)
				->with('concerns', $concerns)->with('hastagproduct',0);
	}

		public function products()
	{
		$categories = DB::table('categories')->where('is_active', 1)->orderBy('order')->get();
		$videos = DB::table('videos')->where('is_active', 1)->orderBy('id', 'DESC')->limit(3)->get();
		$buyContent = DB::table('website_contents')->where('key', 'buy_now_button')->first();
		$officeContentAddress = DB::table('website_contents')->where('key', 'office_address')->first();
		$telephone = DB::table('website_contents')->where('key', 'telephone')->first();
		$concerns = DB::table('website_contents')->where('key', 'concern')->get();

		return view('pages.index')
				->with('categories', $categories)
				->with('videos', $videos)
				->with('buyButton', $buyContent->content)
				->with('office_address', $officeContentAddress->content)
				->with('telephone', $telephone->content)
				->with('concerns', $concerns)->with('hastagproduct',1);
	}

}
