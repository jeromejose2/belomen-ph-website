<?php namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;

use App\User;
use Auth;
use Request;
use App\Audit;

use Input;
use Validator;
use Redirect;
use Session;
use App\Category;
use App\Product;
use File;

class AdminProductsController extends Controller 
{

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
			return view('admin.index'); 
	}

	public function home()
	{

		if(session('admin')['id'] == 0)
		{
			return view('admin.index'); 
		}
		$user_id = session('admin')['id'];

		$categories = DB::table('products')->orderBy('category_id')->orderBy('order')->get();
		
		return view('admin.products')
		->with('title', 'Products')
		->with('products', $categories);
	}

		public function edit($id = null)
	{
		$categories_id = Category::lists('title', 'id');

		if($id != 0)
		{
		$categories = DB::table('products')->where('id', $id)->orderBy('id')->get();

		return view('admin.edit_product')
		->with('title', 'Products')
		->with('categories_id', $categories_id)
		->with('categories', $categories);
		}else
		{
			return view('admin.edit_product')->with('title', 'Products')->with('categories_id', $categories_id);
		}

	}


		public function update()
	{
			$input = Request::all();

			$validator = Validator::make(Request::all(),
				    ['image' => 'mimes:jpeg,bmp,png,jpg,gif']
				   
				);

			if ($validator->fails())
				{
				    session()->put('error', [
							'message'=>'Please upload corret file format.Please try again.'
						]);
				return redirect('admin/products/edit');
				}

			if(isset($input['id']) && $input['id'] != 0)
			{

	
				
					$Category = Product::find($input['id']);
					$Category->title = Request::input('title');
					$Category->description = trim(Request::input('summernote'));
					$Category->price = Request::input('price');
					$Category->content = Request::input('content');
					$Category->analytics = Request::input('analytics');
					$Category->link = Request::input('link');

						$imageName = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName = $input['id'] . '-product-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/products/', $imageName);
							 	$Category->image = 'products/'.$imageName;
							}
					}

					
					$Category->category_id = $input['category_id'];
					$Category->order = $input['order'];
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Edit Successfully.'
						]);
					
			}else
			{

				
					$Category = New Product;
					$Category->title = Request::input('title');
					$Category->description = trim(Request::input('summernote'));
					$Category->price = Request::input('price');
					$Category->content = Request::input('content');
					$Category->analytics = Request::input('analytics');
					$Category->link = Request::input('link');
						$imageName = '';
					if (Request::hasFile('image'))
					{
						if (Request::file('image')->isValid())
							{
								
							    $imageName = '-product-' .Input::file('image')->getClientOriginalName();
							 	$move =   Request::file('image')->move(base_path() . '/public/uploads/products/', $imageName);
						 		$Category->image = 'products/'.$imageName;
							}
					}

				
					$Category->category_id = $input['category_id'];
					$Category->order = $input['order'];
					$Category->is_active = $input['is_active'];
					
					$save = $Category->save();
						session()->put('success', [
							'message'=>'Added Successfully.'
						]);

			}
		
		if($save)
		{
		return redirect('admin/products');	
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
			return redirect('admin/products');	
		}
	}

		public function delete($id)
	{

		$category = Product::find($id);
		if($category)
		{

			$filename_image = public_path().'/uploads/'.$category->image;
			if (File::exists($filename_image))
			{
			    File::delete($filename_image);
			}

			$remove = $category->delete();
			if($remove)
			{
			session()->put('success', [
							'message'=>'You successfully removed the product.'
						]);
			return redirect('admin/products');	
			}
		}else
		{
			session()->put('error', [
							'message'=>'Error in saving.Please try again.'
						]);
		return redirect('admin/products');	
		}
		

	}

}
