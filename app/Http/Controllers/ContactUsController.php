<?php namespace App\Http\Controllers;
use DB;
use Input;
use Mail;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use View;

use App\User;
use Auth;
use Request;
use App\Audit;
use App\CmsUser;
use App\EmailData;


class ContactUsController extends Controller
{

	public function send()
	{
		
		$name = Input::get('name');
		$email = Input::get('email');
		$concern = Input::get('concern');
		$message_form = Input::get('message');
		$input = Request::all();
		if(empty($name) or empty($email) or empty($concern) or empty($message_form)) {
				session()->put('error', [
												'message'=>'Please complete all the fields required.Please try again.'
											]);
				return redirect('/');

		} elseif(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				session()->put('error', [
												'message'=>'Invalid email format. Ex: user@example.com .Please try again.'
											]);
				return redirect('/');

		} else {
			//save to db
					$Category = New EmailData;
					$Category->name = $name;
					$Category->email = $email;
					$Category->concern = $concern;
					$Category->message = $message_form;
					$save = $Category->save();
					$concerns = DB::table('website_contents')->where('key', 'concern')->get();
					

						if($save)
						{

							$email_array = array();
						$email_array[] = DB::table('email_messages')->where('type',  $concern)->get();
						$email_array[] = DB::table('email_sender')->where('type', $concern)->get();

										//send to belo admin
									$email = Mail::send('emails.send_belo',['name'=>$name,'email'=>$email,'concern'=>$concern,'message_form'=>Input::get('message')] , function($message) Use ($email_array)
									{
										$message->subject('Inquiry - '.Input::get('concern'));

									    $message->from($email_array[1][0]->email);

									    $message->to('belomen@iskincareinc.com');//for prod
									    //$message->to('jerome.jose@nuworks.ph');
									});

									//send to User

									$email1 =  Mail::send('emails.contactUs', ['name'=>$name,'body_message'=>trim($email_array[0][0]->content)] , function($message)
										 Use ($email_array)
									{
										$message->subject($email_array[0][0]->subject);

									    $message->from($email_array[1][0]->email);

									    $message->to(Input::get('email'));//for prod
									   // $message->to('jerome.jose@nuworks.ph');
									});

					//POP-UP: Thanks for your message, bro! We'll get back to you as soon as we can. We've sent a confirmation message to your email. 
						session()->put('success', [
							'message'=>'Thanks for your message, bro! We\'ll get back to you as soon as we can. We\'ve sent a confirmation message to your email. '
						]);

									return redirect('/');

						}
						else
							{
								session()->put('error', [
												'message'=>'Error in saving.Please try again.'
											]);
								return redirect('/');	
							}
		}
	}

}
