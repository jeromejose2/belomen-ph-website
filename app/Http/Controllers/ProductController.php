<?php namespace App\Http\Controllers;
use DB;
use Input;

class ProductController extends Controller 
{

	public function getAll()
	{
		$category_id = Input::get('category_id');
		
		$products = DB::table('products')->where('category_id', $category_id)->where('is_active', 1)->orderBy('order', 'ASC')->get();

		echo json_encode($products);
	}

	public function getAll_fbpages()
	{
		$category_id = Input::get('category_id');
		
		$products = DB::table('products')->where('is_active', 1)->orderBy('category_id', 'ASC')->orderBy('order', 'ASC')->get();
		return view('fb.index')
		->with('products', $products);
		//echo json_encode($products);
	}


}