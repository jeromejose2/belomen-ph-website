<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
//Route::get('/products', 'WelcomeController@index');
//Route::get('/fb/amfphp/gateway.php', 'WelcomeController@index');
Route::get('/fb/amfphp/gateway.php', function(){
	return	redirect('/');
});
Route::get('/products', 'WelcomeController@products');
/**start for admin **/
Route::get('/admin', 'AdminController@index');
Route::get('/admin/home', 'AdminController@home');

Route::get('/admin/dashboard', 'AdminController@dashboard');
Route::get('/admin/home/logout', 'AdminController@logout');
Route::get('/admin/videos', 'AdminVideosController@home');
Route::get('/admin/products', 'AdminProductsController@home');
Route::get('/admin/website_contents', 'AdminWebContentController@home');
Route::get('/admin/settings', 'AdminSettingsController@index');
Route::get('/admin/emails', 'AdminEmailsController@index');
Route::get('/admin/emails/sender/', 'AdminEmailsController@sender');
Route::post('/admin', 'AdminController@index');

Route::get('/admin/about_us', 'AdminController@aboutus');
Route::post('/admin/about_us/save',		'AdminController@aboutus_update');

Route::get('/admin/category/edit/{id?}',		'AdminController@edit');
Route::post('/admin/category/update',		'AdminController@update');
Route::get('/admin/category/delete/{id?}',		'AdminController@delete');

Route::get('/admin/videos/edit/{id?}',		'AdminVideosController@edit');
Route::post('/admin/videos/update',		'AdminVideosController@update');
Route::get('/admin/videos/delete/{id?}',		'AdminVideosController@delete');

Route::get('/admin/products/edit/{id?}',		'AdminProductsController@edit');
Route::post('/admin/products/update',		'AdminProductsController@update');
Route::get('/admin/products/delete/{id?}',		'AdminProductsController@delete');

Route::get('/admin/website_contents/edit/{id?}',		'AdminWebContentController@edit');
Route::post('/admin/website_contents/update',		'AdminWebContentController@update');
Route::get('/admin/website_contents/delete/{id?}',		'AdminWebContentController@delete');
Route::get('/admin/website_contents/key/{key?}',		'AdminWebContentController@key');
Route::post('/admin/website_contents/key_update',		'AdminWebContentController@key_update');

Route::post('/admin/faqs/update',		'AdminWebContentController@faq_update');
Route::get('/admin/faqs/edit/{id?}',		'AdminWebContentController@faq_edit');
Route::get('/admin/faqs/new/{id?}',		'AdminWebContentController@faq_edit');



Route::get('/admin/settings/edit/{id?}',		'AdminSettingsController@edit');
Route::post('/admin/settings/update',		'AdminSettingsController@update');
Route::get('/admin/settings/delete/{id?}',		'AdminSettingsController@delete');

Route::get('/admin/emails/edit/{id?}',		'AdminEmailsController@edit');
Route::get('/admin/emails/new/{id?}',		'AdminEmailsController@edit');
Route::post('/admin/emails/update',		'AdminEmailsController@update');
Route::get('/admin/emails/delete/{id?}',		'AdminEmailsController@delete');

Route::get('/admin/emails/sender/edit/{id?}',		'AdminEmailsController@sender_edit');
Route::post('/admin/emails/sender/update',		'AdminEmailsController@sender_update');
Route::get('/admin/emails/sender/delete/{id?}',		'AdminEmailsController@sender_delete');

/**end for admin **/

//Products Route
Route::post('products/getAll', 'ProductController@getAll');
Route::get('products/forfb', 'ProductController@getAll_fbpages');

//Popup route
Route::get('video/update', 'VideoController@update_views');

Route::get('popup/video', function(){
	return View::make('popups.video');
});
Route::get('popup/analyzer', function(){
	return View::make('popups.faceAnalyzer');
});
Route::get('popup/terms-and-condition', function(){
	return View::make('popups.termsCondition')->with('terms', $_GET['terms']);
});
Route::get('popup/privacy-policy', function(){
	return View::make('popups.privacyPolicy')->with('privacy_policy', $_GET['privacy_policy']);
});
Route::get('popup/faq', function(){
	$faqs = DB::table('website_contents')->where('key', 'faq_questions')->get();
	//$faqs_answer = DB::table('website_contents')->where('key', 'faq_answers')->get();
	return View::make('popups.faqs')
					->with('faqs', $faqs)
					->with('faqs_answer', $faqs);
});
Route::get('website_content/get_terms', function(){
	$websiteContents = DB::table('website_contents')->where('key', 'terms_and_condition')->first();
	echo $websiteContents->content;
});
Route::get('website_content/get_privacy', function(){
	$websiteContents = DB::table('website_contents')->where('key', 'privacy_policy')->first();
	echo $websiteContents->content;
});

//Video route
Route::get('videos', 'VideoController@index');
Route::post('videos/filter', 'VideoController@filter');

//About route
Route::get('about', function(){
	return view('pages.about');
});

//Contact Us
Route::post('contactUs/send', 'ContactUsController@send');

//Message Route
Route::get('message/success', function(){
	return view('emails.message');
});


//Search Route
Route::get('search', function(){
	$buyContent = DB::table('website_contents')->where('key', 'buy_now_button')->first();
	$officeContentAddress = DB::table('website_contents')->where('key', 'office_address')->first();
	$telephone = DB::table('website_contents')->where('key', 'telephone')->first();
	$concerns = DB::table('website_contents')->where('key', 'concern')->get();

	return view('pages.search')->with('buyButton', $buyContent->content)
				->with('office_address', $officeContentAddress->content)
				->with('telephone', $telephone->content)
				->with('concerns', $concerns);
});

//Eport
Route::get('export', 'AdminController@export');